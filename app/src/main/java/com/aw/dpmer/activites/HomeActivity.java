package com.aw.dpmer.activites;

import android.support.v4.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.aw.dpmer.R;
import com.aw.dpmer.fragments.AssujettisListFragment;
import com.aw.dpmer.dashboard.DashboardFragment;
import com.aw.dpmer.personneMorale.AddPersonneMoraleFragment;
import com.aw.dpmer.personnePhysique.AddPersonnePhysiqueFragment;
import com.aw.dpmer.utils.Constant;
import com.aw.dpmer.utils.Context;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FragmentManager mFragmentManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        setTitle("Dashboard");
        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.beginTransaction().
                replace(R.id.fragment_container, DashboardFragment.newInstance()).commit();

        Intent intent = getIntent();
        if (intent != null){
            instantiateFragment(intent.getIntExtra(Constant.KEY_DRAWER_ITEM, 0));
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            setTitle("Dashboard");
            mFragmentManager.beginTransaction().
                    replace(R.id.fragment_container, DashboardFragment.newInstance()).commit();
        } else if (id == R.id.nav_personne_physique) {
            setTitle("Personnes physiques");
            mFragmentManager.beginTransaction().
                    replace(R.id.fragment_container, AddPersonnePhysiqueFragment.newInstance()).commit();
        } else if (id == R.id.nav_personne_morale) {
            setTitle("Personnes morales");
            mFragmentManager.beginTransaction().
                    replace(R.id.fragment_container, AddPersonneMoraleFragment.newInstance()).commit();
        } else if (id == R.id.nav_liste) {
            setTitle("Liste des contribuables");
            mFragmentManager.beginTransaction().
                    replace(R.id.fragment_container, AssujettisListFragment.newInstance()).commit();
        } else if (id == R.id.nav_synchronisation) {

            setTitle("Synchronisation");
//            ProgressDialog progressDialog = ProgressDialog.show(this, "", "Loading...", true);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void instantiateFragment(int drawerItemCode){

        mFragmentManager = getSupportFragmentManager();

        switch(drawerItemCode){
            case Constant.CODE_DASHBOARD:
                mFragmentManager.beginTransaction().
                        replace(R.id.fragment_container, DashboardFragment.newInstance()).commit();
                break;
            case Constant.CODE_PERSONNE_PHYSIQUE:
                mFragmentManager.beginTransaction().
                        replace(R.id.fragment_container, AddPersonnePhysiqueFragment.newInstance()).commit();
                break;
            case Constant.CODE_PERSONNE_MORALE:
                mFragmentManager.beginTransaction().
                        replace(R.id.fragment_container, AddPersonneMoraleFragment.newInstance()).commit();
                break;
            case Constant.CODE_LISTE:
                mFragmentManager.beginTransaction().
                        replace(R.id.fragment_container, AssujettisListFragment.newInstance()).commit();
            case Constant.CODE_SYNC:
                // Todo Add some code for synchronisation
            default:
                mFragmentManager.beginTransaction().
                        replace(R.id.fragment_container, DashboardFragment.newInstance()).commit();
                break;
        }
    }
}
