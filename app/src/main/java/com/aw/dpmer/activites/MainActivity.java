package com.aw.dpmer.activites;

import com.aw.dpmer.R;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button personneMoraleBT = findViewById(R.id.personne_morale_bt);
        Button personnePhysiqueBT = findViewById(R.id.personne_physique_bt);

        personneMoraleBT.setOnClickListener(this);
        personnePhysiqueBT.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, BaseActivity.class);
        startActivity(intent);
    }
}
