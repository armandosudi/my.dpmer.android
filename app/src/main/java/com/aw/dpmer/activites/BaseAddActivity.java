package com.aw.dpmer.activites;

import android.support.v4.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.aw.dpmer.R;
import com.aw.dpmer.fragments.ActiviteAddFragment;
import com.aw.dpmer.propriete.ProprieteAddFragment;
import com.aw.dpmer.utils.Constant;

public class BaseAddActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_add);

        FragmentManager fragmentManager = getSupportFragmentManager();

        Intent intent = getIntent();
        int type = intent.getIntExtra(Constant.KEY_TYPE,1);
        int codeAssujetti = intent.getIntExtra(Constant.KEY_CODE_ASSUJETTI, 0);
        switch(type){
            case Constant.ACTIVITE_TYPE:
                fragmentManager.beginTransaction()
                        .add(R.id.fragment_container, ActiviteAddFragment.newInstance(codeAssujetti)).commit();
                break;
            case Constant.PROPRIETE_TYPE:
                fragmentManager.beginTransaction()
                        .add(R.id.fragment_container, ProprieteAddFragment.newInstance(codeAssujetti)).commit();
                break;
        }
    }
}
