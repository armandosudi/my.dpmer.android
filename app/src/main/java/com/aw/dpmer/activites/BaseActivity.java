package com.aw.dpmer.activites;

import android.annotation.TargetApi;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.aw.dpmer.R;
import com.aw.dpmer.fragments.ActiviteListFragment;
import com.aw.dpmer.propriete.ProprieteListFragment;
import com.aw.dpmer.utils.Constant;
import com.aw.dpmer.utils.Context;

import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

        Intent intent = getIntent();
        final int codeAssujetti = intent.getIntExtra(Constant.KEY_CODE_ASSUJETTI, 0);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(BaseActivity.this, BaseAddActivity.class);

                switch (mViewPager.getCurrentItem()){
                    case Constant.ACTIVITE_TYPE:
                        intent.putExtra(Constant.KEY_TYPE, Constant.ACTIVITE_TYPE);
                        intent.putExtra(Constant.KEY_CODE_ASSUJETTI, codeAssujetti);
                        startActivity(intent);
                        break;
                    case Constant.PROPRIETE_TYPE:
                        intent.putExtra(Constant.KEY_TYPE, Constant.PROPRIETE_TYPE);
                        intent.putExtra(Constant.KEY_CODE_ASSUJETTI, codeAssujetti);
                        startActivity(intent);
                        break;
                        default:
                            intent.putExtra(Constant.KEY_TYPE, Constant.PROPRIETE_TYPE);
                            break;
                }
            }
        });

        /*if(Context.CurrentUser != null){
            ((TextView)findViewById(R.id.nomUserTextView)).setText(Context.CurrentUser.getNomComplet());
            ((TextView)findViewById(R.id.emailUserTextView)).setText(Context.CurrentUser.getEmail());
        }*/
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_base, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Intent intent = null;
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            intent = new Intent(BaseActivity.this, HomeActivity.class);
            intent.putExtra(Constant.KEY_DRAWER_ITEM, Constant.CODE_DASHBOARD);
            startActivity(intent);
        } else if (id == R.id.nav_personne_physique) {

            intent = new Intent(BaseActivity.this, HomeActivity.class);
            intent.putExtra(Constant.KEY_DRAWER_ITEM, Constant.CODE_PERSONNE_PHYSIQUE);
            startActivity(intent);
        } else if (id == R.id.nav_personne_morale) {
            intent = new Intent(BaseActivity.this, HomeActivity.class);
            intent.putExtra(Constant.KEY_DRAWER_ITEM, Constant.CODE_PERSONNE_MORALE);
            startActivity(intent);
        } else if (id == R.id.nav_liste) {
            intent = new Intent(BaseActivity.this, HomeActivity.class);
            intent.putExtra(Constant.KEY_DRAWER_ITEM, Constant.CODE_LISTE);
            startActivity(intent);
        } else if (id == R.id.nav_synchronisation) {
            //TODO launch synchronization
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        List<Fragment> mFragmentList = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            mFragmentList.add(ActiviteListFragment.newInstance());
            mFragmentList.add(ProprieteListFragment.newInstance());
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Activites";
                case 1:
                    return "Proprietes";
                default:
                    return null;
            }

        }

        @Override
        public int getCount() {
            // Show 2 total pages.
            return mFragmentList.size();
        }
    }
}
