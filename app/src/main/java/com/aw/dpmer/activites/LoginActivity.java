package com.aw.dpmer.activites;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.aw.dpmer.R;
import com.aw.dpmer.database.DpmerDatabase;
import com.aw.dpmer.repositories.InitConfigData;
import com.aw.dpmer.utils.Constant;
import com.aw.dpmer.utils.Context;

public class LoginActivity extends AppCompatActivity{

    private static final String TAG = "LoginActivity";

    private Button loginButton;
    private EditText email,passWord;

    SharedPreferences mSharedPref;
    SharedPreferences.Editor mEditor;

    public ProgressBar progressBar = null;

    public static final int HOME_ACTIVITY_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        mEditor = mSharedPref.edit();

        loginButton = findViewById(R.id.btnLogin);

        email = findViewById(R.id.email);
        passWord = findViewById(R.id.password);

        DpmerDatabase.getDatabase(getApplicationContext());

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Snackbar.make (v, "Connexion en cours...", Snackbar.LENGTH_LONG)
                        .setAction ("Action", null).show ();

                new AuthentificationTask(v.getContext(),
                        email.getText().toString(), passWord.getText().toString()).execute();
            }
        });
    }

    public void configuration_onclick(View v){

        try {

            Snackbar.make (v, "Configuration en cours...", Snackbar.LENGTH_LONG)
                    .setAction ("Action", null).show ();

            progressBar = findViewById(R.id.progressbar);


            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            InitConfigData initConfig = new InitConfigData(v.getContext());
            initConfig.setProgressBar(progressBar);
            initConfig.execute();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class AuthentificationTask extends AsyncTask<Void, Void, String> {

        private android.content.Context context;

        public String login;
        public String password;

        public AuthentificationTask(android.content.Context context,
                                    String login, String password) {
            this.context = context;
            this.login = login;
            this.password = password;
        }

        @Override
        protected String doInBackground(Void... params) {
            Context.CurrentUser = DpmerDatabase.getInstance().getIUtilisateuDao().getUtilisateur(login, password);
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            if (Context.CurrentUser == null) {
                new AlertDialog.Builder(this.context).setTitle("")
                        .setMessage("Non d'utilisateur et/ou mot de passe incorrect.")
                        .setCancelable(false).setPositiveButton("Ok", null)
                        .show();
                return;
            }

            String codeUtilisateur = Context.CurrentUser.getCodeUtilisateur();
            mEditor.putString(Constant.KEY_CODE_UTILISATEUR, codeUtilisateur).apply();
            mEditor.commit();

            String test = mSharedPref.getString(Constant.KEY_CODE_UTILISATEUR, null);
            Log.e(TAG, "onPostExecute: " + test);

            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
            startActivityForResult(intent, HOME_ACTIVITY_REQUEST_CODE);
        }
    }
}
