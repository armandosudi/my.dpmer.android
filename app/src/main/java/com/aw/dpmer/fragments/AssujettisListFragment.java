package com.aw.dpmer.fragments;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.aw.dpmer.R;
import com.aw.dpmer.database.DpmerDatabase;
import com.aw.dpmer.entites.PersonneMorale;
import com.aw.dpmer.entites.PersonnePhysique;
import com.aw.dpmer.adapters.PersonneMoraleAdapter;
import com.aw.dpmer.adapters.PersonnePhysiqueAdapter;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class AssujettisListFragment extends Fragment {

    List<PersonneMorale> mPersonneMoraleList = new ArrayList<>();
    List<PersonnePhysique> mPersonnePhysiqueList = new ArrayList<>();
    PersonneMoraleAdapter mPersonneMoraleAdapter;
    PersonnePhysiqueAdapter mPersonnePhysiqueAdapter;
    RecyclerView personneMoraleRV;
    Activity mActivity;

    public AssujettisListFragment() {
    }

    public static AssujettisListFragment newInstance() {
        AssujettisListFragment fragment = new AssujettisListFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_assujettis_list, container, false);

        mPersonneMoraleAdapter = new PersonneMoraleAdapter(mActivity, mPersonneMoraleList);
        mPersonnePhysiqueAdapter = new PersonnePhysiqueAdapter(mActivity, mPersonnePhysiqueList);

        personneMoraleRV = view.findViewById(R.id.personne_morale_rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayout.VERTICAL, false);
        personneMoraleRV.setLayoutManager(linearLayoutManager);
        personneMoraleRV.setHasFixedSize(true);
        personneMoraleRV.addItemDecoration(new DividerItemDecoration(mActivity, linearLayoutManager.getOrientation() ));
        personneMoraleRV.setAdapter(mPersonneMoraleAdapter);

        Spinner selectSP = view.findViewById(R.id.select_sp);
        selectSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) parent.getItemAtPosition(position);
                if (item.equals("Personne Physique")){
                    // toggle personne physique
                    findAllPersonnePhysique();
                }
                if (item.equals("Personne Morale")){
                    // toggle personne morale
                    findAllPersonneMorale();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        findAllPersonnePhysique();

        return view;
    }

    public void findAllPersonneMorale(){
        (new AsyncTask<Void, Void, List<PersonneMorale>>(){
            @Override
            protected List<PersonneMorale>doInBackground(Void... voids) {
                List<PersonneMorale> personneMoraleList = DpmerDatabase.getInstance().getIPersonneMoraleDao().getAllPersonneMoraleList();
                Log.e(TAG, "doInBackground: personne morale list size : " + personneMoraleList.size() );
                return personneMoraleList;
            }

            @Override
            protected void onPostExecute(List<PersonneMorale> personneMorales) {
                personneMoraleRV.setAdapter(mPersonneMoraleAdapter);
                mPersonneMoraleAdapter.addPersonneMorale(personneMorales);
                mPersonneMoraleAdapter.notifyDataSetChanged();
            }
        }).execute();
    }

    public void findAllPersonnePhysique() {
        (new AsyncTask<Void, Void, List<PersonnePhysique>>(){
            @Override
            protected List<PersonnePhysique>doInBackground(Void... voids) {
                List<PersonnePhysique> personnePhysiqueList = DpmerDatabase.getInstance().getIPersonnePhysiqueDao().getAllPersonnePhysiqueList();
                Log.e(TAG, "doInBackground: personne physique list size : " + personnePhysiqueList.size() );
                return personnePhysiqueList;
            }

            @Override
            protected void onPostExecute(List<PersonnePhysique> personnePhysiques) {
                personneMoraleRV.setAdapter(mPersonnePhysiqueAdapter);
                mPersonnePhysiqueAdapter.addPersonnePhysique(personnePhysiques);
                mPersonnePhysiqueAdapter.notifyDataSetChanged();
            }
        }).execute();
    }
}

