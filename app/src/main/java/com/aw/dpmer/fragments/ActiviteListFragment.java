package com.aw.dpmer.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.aw.dpmer.R;
import com.aw.dpmer.adapters.ActiviteAdapter;
import com.aw.dpmer.database.DpmerDatabase;
import com.aw.dpmer.entites.Activite;
import com.aw.dpmer.entites.Propriete;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class ActiviteListFragment extends Fragment {

    List<Activite> mActiviteList = new ArrayList<>();
    Activity mActivity;
    ActiviteAdapter mActiviteAdapter;

    public ActiviteListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ActiviteListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ActiviteListFragment newInstance() {
        ActiviteListFragment fragment = new ActiviteListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        mActivity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_activite_list, container, false);

        mActiviteAdapter = new ActiviteAdapter(mActivity, mActiviteList);
        RecyclerView activiteRV = view.findViewById(R.id.activite_rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayout.VERTICAL, false);
        activiteRV.setLayoutManager(linearLayoutManager);
        activiteRV.setHasFixedSize(true);
        activiteRV.addItemDecoration(new DividerItemDecoration(mActivity, linearLayoutManager.getOrientation()));
        activiteRV.setAdapter(mActiviteAdapter);

        findAllActivite();

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void findAllActivite(){
        (new AsyncTask<Void, Void, List<Activite>>(){
            @Override
            protected List<Activite>doInBackground(Void... voids) {
                List<Activite> proprieteList = DpmerDatabase.getInstance().getIActiviteDao().findAllActiviteList();
                Log.e(TAG, "doInBackground: propriete list size : " + proprieteList.size() );
                return proprieteList;
            }

            @Override
            protected void onPostExecute(List<Activite> activites) {
                mActiviteAdapter.addActiviteList(activites);
                mActiviteAdapter.notifyDataSetChanged();
            }
        }).execute();
    }

}
