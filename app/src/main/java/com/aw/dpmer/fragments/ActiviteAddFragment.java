package com.aw.dpmer.fragments;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;

import com.aw.dpmer.R;
import com.aw.dpmer.database.DpmerDatabase;
import com.aw.dpmer.entites.Activite;
import com.aw.dpmer.entites.Assujetti;
import com.aw.dpmer.entites.Cellule;
import com.aw.dpmer.entites.Commune;
import com.aw.dpmer.entites.NatureActivite;
import com.aw.dpmer.entites.Quartier;
import com.aw.dpmer.utils.Constant;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ActiviteAddFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ActiviteAddFragment extends Fragment {

    private Activity mActivity;
    SharedPreferences mSharedPref;
    SharedPreferences.Editor mEditor;
    private View view;

    private Spinner natureActiviteSP, communeSP, quartierSP, celluleSP, droitImmobilierSP;
    private EditText sigleET, adressePostaleET, autreReferenceET, indentificationNationaleET, registreCommerceET, dateInscriptionET, dateDebutActiviteET, motoET, vehiculeET, tracteurET, tourismeET, bateauET, salariesPermanentNationauxET, salariesPermanentExpatrieET, activitePrincipaleET, activiteSecondaireET;
    Button selectRccmBT,selectIdnatBT;
    private String mSigle, mAdressePostale, mAutreReference, mIndentificationNationale, mRegistreCommerce, mActivitePrincipale, mActiviteSecondaire, mDroitPropriete;
    private int mMoto, mVehicule, mTracteur, mTourisme, mBateau, mSalariesPermanentNationaux, mSalariesPermanentExpatrie;
    private Cellule mCellule;
    private Commune mCommune;
    private Quartier mQuartier;
    private NatureActivite mNatureActivite;
    private Date mDateInscription, mDateDebutActivite;

    private int mCodeAssujetti;

    private Calendar mCalendar = Calendar.getInstance();


    public ActiviteAddFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ActiviteAddFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ActiviteAddFragment newInstance(int codeAssujetti) {
        ActiviteAddFragment fragment = new ActiviteAddFragment();
        Bundle args = new Bundle();
        args.putInt(Constant.KEY_CODE_ASSUJETTI, codeAssujetti);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mCodeAssujetti = args.getInt(Constant.KEY_CODE_ASSUJETTI);

        mActivity = getActivity();
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
//        mSharedPref = mActivity.getPreferences(Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activite_add, container, false);

        initView(view);

        populateNatureActivite();
        populateCommunes();

        Button saveBT = view.findViewById(R.id.save_bt);
        saveBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collectData();
            }
        });

        return view;
    }

    public void initView(View view){
        sigleET = view.findViewById(R.id.sigle_et);
        adressePostaleET = view.findViewById(R.id.adresse_postale_et);
        autreReferenceET = view.findViewById(R.id.autre_reference_et);
        indentificationNationaleET = view.findViewById(R.id.identification_nationale_et);
        registreCommerceET = view.findViewById(R.id.numero_registre_commerce_et);
        dateInscriptionET = view.findViewById(R.id.date_inscription_et);
        dateDebutActiviteET = view.findViewById(R.id.date_debut_et);
        motoET = view.findViewById(R.id.motos_et);
        vehiculeET = view.findViewById(R.id.vehicules_utilitaires_et);
        tracteurET = view.findViewById(R.id.tracteurs_et);
        tourismeET = view.findViewById(R.id.vehicule_tourisme_et);
        bateauET = view.findViewById(R.id.bateaux_et);
        salariesPermanentNationauxET = view.findViewById(R.id.salarie_permanent_nationaux_et);
        salariesPermanentExpatrieET = view.findViewById(R.id.salarie_permanent_expatries_et);
        activitePrincipaleET = view.findViewById(R.id.activite_principale_et);
        activiteSecondaireET = view.findViewById(R.id.activite_secondaire_et);

        natureActiviteSP = view.findViewById(R.id.nature_activite_sp);
        communeSP = view.findViewById(R.id.commune_sp);
        quartierSP = view.findViewById(R.id.quartier_sp);
        celluleSP = view.findViewById(R.id.cellule_sp);
        droitImmobilierSP = view.findViewById(R.id.droit_immobilier_sp);

        final DatePickerDialog.OnDateSetListener date1 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, month);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                mDateDebutActivite = updateLabel(dateDebutActiviteET);
            }
        };
        dateDebutActiviteET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(mActivity, date1, mCalendar
                        .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                        mCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener date2 = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, month);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                mDateInscription = updateLabel(dateInscriptionET);
            }
        };
        dateInscriptionET.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(mActivity, date2, mCalendar
                        .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                        mCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        communeSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCommune = (Commune)parent.getItemAtPosition(position);
                populateQuartiers(mCommune.codeCommune);
                populateCellules(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        quartierSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mQuartier = (Quartier)parent.getItemAtPosition(position);
                populateCellules(mQuartier.codeQuartier);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        natureActiviteSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mNatureActivite = (NatureActivite)parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        droitImmobilierSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mDroitPropriete = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        selectRccmBT = view.findViewById(R.id.rccm_bt);
        selectIdnatBT = view.findViewById(R.id.idnat_bt);



    }

    public void collectData() {
        mSigle = sigleET.getText().toString();
        mAdressePostale = adressePostaleET.getText().toString();
        mAutreReference = autreReferenceET.getText().toString();
        mIndentificationNationale = indentificationNationaleET.getText().toString();
        mRegistreCommerce = registreCommerceET.getText().toString();
//      mDateInscription = dateInscriptionET.getText().toString();
//      dateDebutActivite = dateDebutActiviteET.getText().toString();
        mMoto = Integer.parseInt(motoET.getText().toString());
        mVehicule = Integer.parseInt(vehiculeET.getText().toString());
        mTracteur = Integer.parseInt(tracteurET.getText().toString());
        mTourisme = Integer.parseInt(tourismeET.getText().toString());
        mBateau = Integer.parseInt(bateauET.getText().toString());
        mSalariesPermanentNationaux = Integer.parseInt(salariesPermanentNationauxET.getText().toString());
        mSalariesPermanentExpatrie = Integer.parseInt(salariesPermanentExpatrieET.getText().toString());
        mActivitePrincipale = activitePrincipaleET.getText().toString();
        mActiviteSecondaire = activiteSecondaireET.getText().toString();

        //TODO recuperer les uri pour RCCM, IDNAT

        String codeUtilisateur = mSharedPref.getString(Constant.KEY_CODE_UTILISATEUR, null);

        Date dateEnregistrement = mCalendar.getTime();

        Activite activite = new Activite(mCodeAssujetti, mNatureActivite.getCodeNatureActivite(), mSigle, mAutreReference, mCellule.getCodeCellule(), mAdressePostale, mIndentificationNationale, mRegistreCommerce, mDateInscription, mDateDebutActivite, mMoto, mVehicule, mTourisme, mTracteur, mBateau, mDroitPropriete, mSalariesPermanentNationaux, mSalariesPermanentExpatrie, "url_rccm", "url_idnat", dateEnregistrement, codeUtilisateur, mActivitePrincipale, mActiviteSecondaire );

        insertData(activite, codeUtilisateur);
    }

    /**
     * Pour inserer l'activite, il faut d'abord recuperer le codeUitilsateur du telephone puis, recuperer
     * l'Assujetti qui correspond a ce code la, ensuite utiliser le mCodeAssujetti pour inserer l'activite
     * @param activite
     * @param codeUtilisateur
     */
    public void insertData(final Activite activite, final String codeUtilisateur){
        (new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                long[] rowIdActivite = DpmerDatabase.getInstance().getIActiviteDao().insert(activite);

                Log.e(TAG, "collectData: ACTIVITE ROW ID: " + rowIdActivite.length + " " + rowIdActivite[0] + " ");
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }).execute();
    }

    public Date updateLabel(EditText editText){
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRANCE);

        editText.setText(sdf.format(mCalendar.getTime()));
        return mCalendar.getTime();
    }

    private void populateNatureActivite(){
        (new AsyncTask<Void, Void, List<NatureActivite>>(){

            @Override
            protected List<NatureActivite> doInBackground(Void... voids) {
                return DpmerDatabase.getInstance().getINatureActiviteDao().getAllNatureActivite();
            }

            @Override
            protected void onPostExecute(List<NatureActivite> natureActivites) {
                super.onPostExecute(natureActivites);

                if(natureActivites != null && natureActivites.size() > 0) {
                    natureActiviteSP.setAdapter(new ArrayAdapter(
                            mActivity, android.R.layout.simple_spinner_item,
                            natureActivites));
                }
            }
        }).execute();
    }

    private void populateCommunes(){
        (new AsyncTask<Void, Void, List<Commune>>(){

            @Override
            protected List<Commune> doInBackground(Void... voids) {
                return DpmerDatabase.getInstance().getICommuneDao().getAllCommunes();
            }

            @Override
            protected void onPostExecute(List<Commune> communes) {
                super.onPostExecute(communes);

                if(communes != null && communes.size() > 0) {
                    communeSP.setAdapter(new ArrayAdapter(
                            mActivity, android.R.layout.simple_spinner_item,
                            communes));
                }
            }
        }).execute();
    }

    private void populateQuartiers(String selectedCommuneCode){

        if(selectedCommuneCode == null){
            quartierSP.setAdapter(null);
            return;
        }

        (new AsyncTask<String, Void, List<Quartier>>(){

            @Override
            protected List<Quartier> doInBackground(String... values) {
                return DpmerDatabase.getInstance().getIQuartierDao().findQuartierByCode(values[0]);
            }

            @Override
            protected void onPostExecute(List<Quartier> quartiers) {
                super.onPostExecute(quartiers);

                if(quartiers != null && quartiers.size() > 0) {
                    quartierSP.setAdapter(new ArrayAdapter(
                            mActivity, android.R.layout.simple_spinner_item,
                            quartiers));
                }
            }
        }).execute(selectedCommuneCode);
    }

    private void populateCellules(String selectedQuartierCode){

        if(selectedQuartierCode == null){
            celluleSP.setAdapter(null);
            return;
        }

        (new AsyncTask<String, Void, List<Cellule>>(){

            @Override
            protected List<Cellule> doInBackground(String... values) {
                return DpmerDatabase.getInstance().getICelluleDao().findCelluleByCode(values[0]);
            }

            @Override
            protected void onPostExecute(List<Cellule> cellules) {
                super.onPostExecute(cellules);

                if(cellules != null && cellules.size() > 0) {
                    celluleSP.setAdapter(new ArrayAdapter(
                            mActivity, android.R.layout.simple_spinner_item,
                            cellules));
                    celluleSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mCellule = (Cellule) parent.getItemAtPosition(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        }).execute(selectedQuartierCode);
    }

}
