package com.aw.dpmer.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aw.dpmer.R;

import java.util.ArrayList;

public class SpinnerAdapter extends ArrayAdapter<String> {

    private ArrayList mData;
    public Resources mResources;
    private LayoutInflater mInflater;


    public SpinnerAdapter(
            Activity activitySpinner,
            int textViewResourceId,
            ArrayList objects,
            Resources resLocal
    ) {
        super(activitySpinner, textViewResourceId, objects);

        mData = objects;
        mResources = resLocal;
        mInflater = (LayoutInflater) activitySpinner.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent) {

        View view = mInflater.inflate(R.layout.spinner_item, parent, false);
        TextView label = view.findViewById(R.id.list_item);
        label.setText(mData.get(position).toString());

        view.setTag(R.string.meta_position, Integer.toString(position));
        view.setTag(R.string.meta_title, mData.get(position).toString());

        return view;
    }
}