package com.aw.dpmer.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aw.dpmer.R;
import com.aw.dpmer.entites.Activite;

import java.util.ArrayList;
import java.util.List;

public class ActiviteAdapter extends RecyclerView.Adapter<ActiviteAdapter.VH> {

    Activity mActivity;
    List<Activite> mActiviteList = new ArrayList<>();

    static class VH extends RecyclerView.ViewHolder{
        TextView sigleTV, rccmTV, idnatTV;
        public VH(View view){
            super(view);
            sigleTV = view.findViewById(R.id.sigle_tv);
            rccmTV = view.findViewById(R.id.rccm_tv);
            idnatTV = view.findViewById(R.id.idnat_tv);
        }
    }

    public ActiviteAdapter(Activity mActivity, List<Activite> mActiviteList) {
        this.mActivity = mActivity;
        this.mActiviteList = mActiviteList;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        Activite activite = mActiviteList.get(position);

        holder.sigleTV.setText(activite.getSigle());
        holder.rccmTV.setText(activite.getRccm());
        holder.idnatTV.setText(activite.getIdnat());
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activite_item, parent, false);

        return new VH(view);
    }

    public void addActiviteList(List<Activite> activites){
        mActiviteList.addAll(activites);
    }

    @Override
    public int getItemCount() {
        return mActiviteList.size();
    }
}
