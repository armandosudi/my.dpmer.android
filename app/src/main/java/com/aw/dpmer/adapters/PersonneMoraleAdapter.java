package com.aw.dpmer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aw.dpmer.R;
import com.aw.dpmer.entites.PersonneMorale;

import java.util.List;

public class PersonneMoraleAdapter extends RecyclerView.Adapter<PersonneMoraleAdapter.VH> {

    private Context mContext;
    private List<PersonneMorale> mPersonneMoraleList;

    static class VH extends RecyclerView.ViewHolder{
        TextView raisonSocialeTV, sigleTV, formeJuridiqueTV;

        public VH(View view){
            super(view);
            raisonSocialeTV = view.findViewById(R.id.raison_sociale_tv);
            sigleTV = view.findViewById(R.id.sigle_tv);
            formeJuridiqueTV = view.findViewById(R.id.forme_juridique_tv);
        }
    }

    public PersonneMoraleAdapter(Context context, List<PersonneMorale> personneMoraleList) {
        this.mContext = context;
        this.mPersonneMoraleList = personneMoraleList;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View personneMoraleView = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.personne_morale_item, parent, false);

        return new PersonneMoraleAdapter.VH(personneMoraleView);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {

        PersonneMorale personneMorale = mPersonneMoraleList.get(position);

        holder.raisonSocialeTV.setText(personneMorale.getRaisonSociale());
        holder.sigleTV.setText(personneMorale.getSigle());
        holder.formeJuridiqueTV.setText(personneMorale.getCodeFormeJuridique());

    }

    public void addPersonneMorale(List<PersonneMorale> p){
        this.mPersonneMoraleList.addAll(p);
    }

    @Override
    public int getItemCount() {
        return mPersonneMoraleList.size();
    }
}
