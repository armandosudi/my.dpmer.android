package com.aw.dpmer.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aw.dpmer.R;
import com.aw.dpmer.entites.Activite;
import com.aw.dpmer.entites.Propriete;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class ProprieteAdapter extends RecyclerView.Adapter<ProprieteAdapter.VH> {

    Activity mActivity;
    List<Propriete> mProprieteList = new ArrayList<>();

    static class VH extends RecyclerView.ViewHolder {
        TextView referenceTV, superficieTV;
        public VH(View view){
            super(view);
            referenceTV = view.findViewById(R.id.reference_tv);
            superficieTV = view.findViewById(R.id.superficie_tv);
        }
    }

    public ProprieteAdapter(Activity mActivity, List<Propriete> mProprieteList) {
        this.mActivity = mActivity;
        this.mProprieteList = mProprieteList;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        Propriete propriete= mProprieteList.get(position);

        holder.referenceTV.setText(propriete.getReference());
        holder.superficieTV.setText(String.valueOf(propriete.getSuperficie()));
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.propriete_item, parent, false);

        return new VH(view);
    }

    @Override
    public int getItemCount() {
        return mProprieteList.size();
    }

    public void addProprieteList(List<Propriete> proprietes){
        mProprieteList.addAll(proprietes);
    }
}
