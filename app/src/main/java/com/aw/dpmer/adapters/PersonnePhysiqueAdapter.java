package com.aw.dpmer.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aw.dpmer.R;
import com.aw.dpmer.entites.PersonnePhysique;

import java.util.List;

public class PersonnePhysiqueAdapter extends RecyclerView.Adapter<PersonnePhysiqueAdapter.VH> {

    private Context mContext;
    private List<PersonnePhysique> mPersonnePhysiqueList;

    static class  VH extends RecyclerView.ViewHolder {
       TextView nomCompletTV, sexeTV, pieceIdentiteTV, numeroPieceIdentiteTV;

       public VH(View view) {
           super(view);
           nomCompletTV = view.findViewById(R.id.nom_complet_tv);
           sexeTV = view.findViewById(R.id.sexe_tv);
           pieceIdentiteTV = view.findViewById(R.id.piece_identite_tv);
           numeroPieceIdentiteTV = view.findViewById(R.id.numero_piece_identite_tv);
        }
    }

    public PersonnePhysiqueAdapter(Context mContext, List<PersonnePhysique> personnePhysiques) {
        this.mContext = mContext;
        this.mPersonnePhysiqueList = personnePhysiques;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.personne_physique_item, parent, false);

        return new PersonnePhysiqueAdapter.VH(view);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {

        PersonnePhysique personnePhysique = mPersonnePhysiqueList.get(position);

        holder.nomCompletTV.setText(personnePhysique.getNom() + " " + personnePhysique.getPostnom() + " " + personnePhysique.getPrenom());
        holder.sexeTV.setText(personnePhysique.getSexe());
        holder.pieceIdentiteTV.setText(personnePhysique.getPieceIdentite());
        holder.numeroPieceIdentiteTV.setText(personnePhysique.getNumeroPieceIdentite());
    }

    public void addPersonnePhysique(List<PersonnePhysique> personnePhysiques) {
        this.mPersonnePhysiqueList.addAll(personnePhysiques);
    }

    @Override
    public int getItemCount() {
        return mPersonnePhysiqueList.size();
    }
}
