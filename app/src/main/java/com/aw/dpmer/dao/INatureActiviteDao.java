package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.NatureActivite;

import java.util.List;

@Dao
public interface INatureActiviteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(NatureActivite...natureActivite);

    @Update
    void update(NatureActivite...natureActivite);

    @Delete
    void delete(NatureActivite...natureActivite);

    @Query("SELECT * FROM NATURE_ACTIVITE")
    List<NatureActivite> getAllNatureActivite();

    @Query("SELECT * FROM NATURE_ACTIVITE WHERE CODE_NATURE_ACTIVITE=:codeNatureActivite")
    LiveData<List<NatureActivite>> findNatureActiviteByCode(final String codeNatureActivite);

    @Query("DELETE FROM NATURE_ACTIVITE")
    void deleteAll();
}
