package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.TypeBatiment;

import java.util.List;

@Dao
public interface ITypeBatimentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(TypeBatiment...typeBatiment);

    @Update
    void update(TypeBatiment...typeBatiment);

    @Delete
    void delete(TypeBatiment...typeBatiment);

    @Query("SELECT * FROM TYPE_BATIMENT")
    LiveData<List<TypeBatiment>> getAllTypeBatiment();

    @Query("SELECT * FROM TYPE_BATIMENT")
    List<TypeBatiment> getAllTypeBatimentList();

    @Query("SELECT * FROM TYPE_BATIMENT WHERE CODE_TYPE_BATIMENT=:codeTypeBatiment")
    LiveData<List<TypeBatiment>> findTypeBatimentByCode(final String codeTypeBatiment);

    @Query("DELETE FROM TYPE_BATIMENT")
    void deleteAll();
}
