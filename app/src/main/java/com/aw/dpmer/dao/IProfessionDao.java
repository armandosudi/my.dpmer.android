package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.Profession;

import java.util.List;

@Dao
public interface IProfessionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Profession...profession);

    @Update
    void update(Profession...profession);

    @Delete
    void delete(Profession...profession);

    @Query("SELECT * FROM PROFESSION")
    List<Profession> getAllProfessions();

    @Query("SELECT * FROM PROFESSION WHERE CODE_PROFESSION=:codeProfession")
    LiveData<List<Profession>> findProfessionByCode(final String codeProfession);

    @Query("DELETE FROM PROFESSION")
    void deleteAll();
}
