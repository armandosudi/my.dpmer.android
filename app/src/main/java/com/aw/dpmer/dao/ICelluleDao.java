package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.Cellule;

import java.util.List;

@Dao
public interface ICelluleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Cellule...cellule);

    @Update
    void update(Cellule...cellule);

    @Delete
    void delete(Cellule...cellule);

    @Query("SELECT * FROM CELLULE")
    LiveData<List<Cellule>> getAllCellule();

    @Query("SELECT * FROM CELLULE WHERE CODE_QUARTIER=:codeQuartier")
    List<Cellule> findCelluleByCode(final String codeQuartier);

    @Query("DELETE FROM CELLULE")
    void deleteAll();
}
