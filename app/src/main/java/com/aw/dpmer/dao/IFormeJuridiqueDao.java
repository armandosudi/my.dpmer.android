package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.FormeJuridique;
import com.aw.dpmer.entites.PersonneMorale;

import java.util.List;

/**
 * Created by jean.olenga on 27/03/2018.
 */
@Dao
public interface IFormeJuridiqueDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(FormeJuridique...formeJuridique);

    @Update
    void update(FormeJuridique...formeJuridique);

    @Delete
    void delete(FormeJuridique...formeJuridique);

    @Query("SELECT * FROM FORME_JURIDIQUE")
    List<FormeJuridique> getAllFormesJuridiques();

    @Query("DELETE FROM FORME_JURIDIQUE" +
            "")
    void deleteAll();

}