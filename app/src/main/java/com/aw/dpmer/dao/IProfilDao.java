package com.aw.dpmer.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.Profil;
import com.aw.dpmer.entites.Utilisateur;

import java.util.List;

/**
 * Created by jean.olenga on 19/03/2018.
 */
@Dao
public interface IProfilDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Profil...profil);

    @Update
    void update(Profil...profil);

    @Delete
    void delete(Profil...profil);

    @Query("DELETE FROM PROFIL")
    void deleteAll();

    @Query("SELECT * FROM PROFIL")
    List<Profil> getAllProfil();

    @Query("SELECT * FROM PROFIL where LIBELLE =:libelle")
    Profil getProfil(final String libelle);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertList(List<Profil> profilList);
}
