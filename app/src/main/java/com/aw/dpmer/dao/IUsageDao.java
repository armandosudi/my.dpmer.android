package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.Usage;

import java.util.List;

@Dao
public interface IUsageDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Usage...usage);

    @Update
    void update(Usage...usage);

    @Delete
    void delete(Usage...usage);

    @Query("SELECT * FROM USAGE")
    LiveData<List<Usage>> getAllUsage();

    @Query("SELECT * FROM USAGE")
    List<Usage> getAllUsageList();

    @Query("SELECT * FROM USAGE WHERE CODE_USAGE=:codeUsage")
    LiveData<List<Usage>> findUsageByCode(final String codeUsage);

    @Query("DELETE FROM USAGE")
    void deleteAll();
}
