package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.Succursale;

import java.util.List;

@Dao
public interface ISuccursaleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Succursale...succursale);

    @Update
    void update(Succursale...succursale);

    @Delete
    void delete(Succursale...succursale);

    @Query("SELECT * FROM SUCCURSALE")
    LiveData<List<Succursale>> getAllSuccursale();

    @Query("SELECT * FROM SUCCURSALE WHERE CODE_SUCCURSALE=:codeSuccursale")
    LiveData<List<Succursale>> findSuccursaleByCode(final String codeSuccursale);

    @Query("DELETE FROM SUCCURSALE")
    void deleteAll();
}
