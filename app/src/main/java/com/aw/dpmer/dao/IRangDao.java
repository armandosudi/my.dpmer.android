package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.Rang;

import java.util.List;

@Dao
public interface IRangDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Rang...rang);

    @Update
    void update(Rang...rang);

    @Delete
    void delete(Rang...rang);

    @Query("SELECT * FROM RANG")
    LiveData<List<Rang>> getAllRang();

    @Query("SELECT * FROM RANG WHERE CODE_RANG=:codeRang")
    LiveData<List<Rang>> findRangByCode(final String codeRang);

    @Query("DELETE FROM RANG")
    void deleteAll();
}
