package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import com.aw.dpmer.entites.PersonneMorale;

import java.util.List;

/**
 * Created by jean.olenga on 22/03/2018.
 */
@Dao
public interface IPersonneMoraleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insert(PersonneMorale...personneMorale);

    @Update
    void update(PersonneMorale...personneMorale);

    @Delete
    void delete(PersonneMorale...personneMorale);

    @Query("SELECT * FROM PERSONNE_MORALE")
    LiveData<List<PersonneMorale>> getAllPersonneMorale();

    @Query("SELECT * FROM PERSONNE_MORALE")
    List<PersonneMorale> getAllPersonneMoraleList();

    @Query("DELETE FROM PERSONNE_MORALE")
    void deleteAll();

}
