package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.*;

import com.aw.dpmer.entites.Assujetti;
import java.util.List;
/**
 * Created by jean.olenga on 16/03/2018.
 */

@Dao
public interface IAssujettiDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insert(Assujetti...assujetti);

    @Update
    void update(Assujetti...assujetti);

    @Delete
    void delete(Assujetti...assujetti);

    @Query("SELECT CODE_ASSUJETTI FROM ASSUJETTI ORDER BY CODE_ASSUJETTI DESC LIMIT 1")
    int getLastAssujetti();

    @Query("SELECT * FROM ASSUJETTI")
    LiveData<List<Assujetti>> getAllAssujetti();

    @Query("SELECT * FROM ASSUJETTI WHERE CODE_ASSUJETTI=:codeAssujetti")
    LiveData<List<Assujetti>> findAssujettiByCode(final String codeAssujetti);

    @Query("SELECT * FROM ASSUJETTI WHERE CODE_UTILISATEUR=:codeUtilisateur")
    Assujetti findAssujettiByCodeUtilisateur(final String codeUtilisateur);

    @Query("DELETE FROM ASSUJETTI")
    void deleteAll();
}
