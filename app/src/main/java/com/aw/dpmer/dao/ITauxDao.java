package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.Taux;

import java.util.List;

@Dao
public interface ITauxDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Taux...taux);

    @Update
    void update(Taux...taux);

    @Delete
    void delete(Taux...taux);

    @Query("SELECT * FROM TAUX")
    LiveData<List<Taux>> getAllTaux();

    @Query("SELECT * FROM TAUX WHERE CODE_TAUX=:codeTaux")
    LiveData<List<Taux>> findTauxByCode(final String codeTaux);

    @Query("DELETE FROM TAUX")
    void deleteAll();
}
