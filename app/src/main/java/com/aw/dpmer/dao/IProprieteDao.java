package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.Propriete;

import java.util.List;

@Dao
public interface IProprieteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insert(Propriete...propriete);

    @Update
    void update(Propriete...propriete);

    @Delete
    void delete(Propriete...propriete);

    @Query("SELECT * FROM PROPRIETE")
    LiveData<List<Propriete>> getAllPropriete();

    @Query("SELECT * FROM PROPRIETE")
    List<Propriete> getAllProprieteList();

    @Query("SELECT * FROM PROPRIETE WHERE CODE_PROPRIETE=:codePropriete")
    LiveData<List<Propriete>> findProprieteByCode(final String codePropriete);

    @Query("DELETE FROM PROPRIETE")
    void deleteAll();
}
