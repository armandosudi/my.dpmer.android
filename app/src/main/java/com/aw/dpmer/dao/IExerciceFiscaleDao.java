package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.ExerciceFiscale;

import java.util.List;

@Dao
public interface IExerciceFiscaleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ExerciceFiscale...execerciceFiscale);

    @Update
    void update(ExerciceFiscale...commune);

    @Delete
    void delete(ExerciceFiscale...commune);

    @Query("SELECT * FROM EXERCICE_FISCALE")
    LiveData<List<ExerciceFiscale>> getAllExerciceFiscale();

    @Query("SELECT * FROM EXERCICE_FISCALE WHERE ANNEE_FISCALE=:anneeExerciceFiscale")
    LiveData<List<ExerciceFiscale>> findExerciceFiscaleByAnnee(final String anneeExerciceFiscale);

    @Query("DELETE FROM EXERCICE_FISCALE")
    void deleteAll();
}
