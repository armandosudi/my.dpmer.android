package com.aw.dpmer.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.Activite;
import com.aw.dpmer.entites.Assujetti;

import java.util.List;

@Dao
public interface IActiviteDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insert(Activite...activite);

    @Update
    void update(Activite...activite);

    @Delete
    void delete(Activite...activite);

    @Query("SELECT * FROM ACTIVITE WHERE CODE_ASSUJETTI=:codeAssujetti")
    List<Activite> findAllActiviteByAssujettiCode(int codeAssujetti);

    @Query("SELECT * FROM ACTIVITE ")
    List<Activite> findAllActiviteList();

    @Query("DELETE FROM ASSUJETTI")
    void deleteAll();
}
