package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.Quartier;

import java.util.List;

@Dao
public interface IQuartierDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Quartier...quartier);

    @Update
    void update(Quartier...quartier);

    @Delete
    void delete(Quartier...quartier);

    @Query("SELECT * FROM QUARTIER")
    List<Quartier> getAllQuartier();

    @Query("SELECT * FROM QUARTIER WHERE CODE_COMMUNE=:codeCommune")
    List<Quartier> findQuartierByCode(final String codeCommune);

    @Query("DELETE FROM QUARTIER")
    void deleteAll();
}

