package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.ProprieteLocataire;

import java.util.List;

@Dao
public interface IProprieteLocataireDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(ProprieteLocataire...proprieteLocataire);

    @Update
    void update(ProprieteLocataire...proprieteLocataire);

    @Delete
    void delete(ProprieteLocataire...proprieteLocataire);

    @Query("SELECT * FROM PROPRIETE_LOCATAIRE")
    LiveData<List<ProprieteLocataire>> getAllProprieteLocataire();

    @Query("SELECT * FROM PROPRIETE_LOCATAIRE WHERE CODE_PROPRIETE_LOCATAIRE=:codeProprieteLocataire")
    LiveData<List<ProprieteLocataire>> findProprieteLocataireByCode(final String codeProprieteLocataire);

    @Query("DELETE FROM PROPRIETE_LOCATAIRE")
    void deleteAll();
}
