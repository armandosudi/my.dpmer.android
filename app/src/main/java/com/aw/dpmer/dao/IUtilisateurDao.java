package com.aw.dpmer.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.Assujetti;
import com.aw.dpmer.entites.Utilisateur;

import java.util.List;

/**
 * Created by jean.olenga on 18/03/2018.
 */
@Dao
public interface IUtilisateurDao {
    @Insert(onConflict = OnConflictStrategy.ABORT)
    void insert(Utilisateur...utilisateur);

    @Update
    void update(Utilisateur...utilisateur);

    @Delete
    void delete(Utilisateur...utilisateur);

    @Query("DELETE FROM UTILISATEUR")
    void deleteAll();

    @Query("SELECT * FROM UTILISATEUR")
    List<Utilisateur> getAllUtilisateur();

    @Query("SELECT * FROM UTILISATEUR where EMAIL =:email AND PASSWORD =:passWord")
    Utilisateur getUtilisateur(final String email, final String passWord);

}
