package com.aw.dpmer.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.aw.dpmer.entites.PersonneMorale;
import com.aw.dpmer.entites.PersonnePhysique;

import java.util.List;

/**
 * Created by jean.olenga on 27/03/2018.
 */
@Dao
public interface IPersonnePhysiqueDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insert(PersonnePhysique...personnePhysique);

    @Update
    void update(PersonnePhysique...personnePhysique);

    @Delete
    void delete(PersonnePhysique...personnePhysique);

    @Query("SELECT * FROM PERSONNE_PHYSIQUE")
    LiveData<List<PersonnePhysique>> getAllPersonnePhysique();

    @Query("SELECT * FROM PERSONNE_PHYSIQUE")
    List<PersonnePhysique> getAllPersonnePhysiqueList();

    @Query("DELETE FROM PERSONNE_PHYSIQUE")
    void deleteAll();

}
