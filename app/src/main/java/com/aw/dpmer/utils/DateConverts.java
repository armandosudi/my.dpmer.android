package com.aw.dpmer.utils;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Created by jean.olenga on 16/03/2018.
 */

public  class DateConverts {

    @TypeConverter
    public static Date toDate(long dateLong) {
        return new Date(dateLong);
    }

    @TypeConverter
    public static long fromDate(Date date) {
        return date.getTime();
    }
}
