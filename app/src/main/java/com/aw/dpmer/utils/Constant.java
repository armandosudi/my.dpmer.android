package com.aw.dpmer.utils;

public class Constant {

    public static String KEY_TYPE = "key_type";
    public static final String KEY_DRAWER_ITEM = "drawer_item";
    public static final String KEY_CODE_UTILISATEUR = "code_utilisateur";
    public static final String KEY_CODE_ASSUJETTI = "code_assujetti";
    public static final int CODE_DASHBOARD = 1;
    public static final int CODE_PERSONNE_PHYSIQUE = 2;
    public static final int CODE_PERSONNE_MORALE = 3;
    public static final int CODE_LISTE = 4;
    public static final int CODE_SYNC = 5;
    public final static int ACTIVITE_TYPE = 0;
    public final static int PROPRIETE_TYPE = 1;
}
