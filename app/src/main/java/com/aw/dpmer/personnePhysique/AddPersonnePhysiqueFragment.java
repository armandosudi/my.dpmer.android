package com.aw.dpmer.personnePhysique;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.aw.dpmer.activites.BaseActivity;
import com.aw.dpmer.R;
import com.aw.dpmer.database.DpmerDatabase;
import com.aw.dpmer.entites.Assujetti;
import com.aw.dpmer.entites.Cellule;
import com.aw.dpmer.entites.Commune;
import com.aw.dpmer.entites.FormeJuridique;
import com.aw.dpmer.entites.PersonnePhysique;
import com.aw.dpmer.entites.Profession;
import com.aw.dpmer.entites.Quartier;
import com.aw.dpmer.utils.Constant;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;

public class AddPersonnePhysiqueFragment extends Fragment{

    private Activity mActivity;
    private View view;
    private Spinner professionSpinner, communeSpinner, quartierSpinner, celluleSpinner, pieceIdentiteSpinner, mSexeSP;
    private EditText mNfiET, mNomET, mPostnomET, mPrenomET, mDateNaissanceET, mLieuNaissanceET, mMobileOneET, mMobileTwoET, mAdresseElectroniqueET, mNumeroPieceidentiteET, mNomPereET, mNomMereET, mAdressePostaleET, mDetailsAdresseET;
    private TextInputLayout mNifTI, mNomTI, mPostnomTI, mMobileOneTI, mAutreReferenceTI;
    private String mNfi, mNom, mPostnom, mPrenom, mLieuNaissance, mMobileOne, mMobileTwo, mAdresseElectronique, mNumeroPieceidentite, mNomPere, mNomMere, mAdressePostale, mDetailsAdresse, mTypePieceIdentite, mSexe;
    private Date mDateNaissance;
    private TextView dateLabelTV;
    private Calendar mCalendar = Calendar.getInstance();

    private Commune mCommune;
    private  Quartier mQuartier;
    private  Cellule mCellule;
    private  Profession mProfession;

    SharedPreferences mSharedPref;

    public AddPersonnePhysiqueFragment() {
    }

    public static AddPersonnePhysiqueFragment newInstance() {
        AddPersonnePhysiqueFragment fragment = new AddPersonnePhysiqueFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_personne_physique, container, false);

        initView(view);

        professionSpinner = view.findViewById(R.id.profession_sp);
        communeSpinner = view.findViewById(R.id.commune_sp);
        quartierSpinner = view.findViewById(R.id.quartier_sp);
        celluleSpinner = view.findViewById(R.id.cellule_sp);
        pieceIdentiteSpinner = view.findViewById(R.id.pieces_identite_sp);
        mSexeSP = view.findViewById(R.id.sexe_sp);

        populateProfessions();
        populateCommunes();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, month);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        ImageButton dateImageButton = view.findViewById(R.id.date_two_bt);
        dateImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(mActivity, date, mCalendar
                        .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                        mCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        communeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCommune = (Commune)parent.getItemAtPosition(position);
                populateQuartiers(mCommune.codeCommune);
                populateCellules(null);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        quartierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mQuartier = (Quartier)parent.getItemAtPosition(position);
                populateCellules(mQuartier.codeQuartier);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        pieceIdentiteSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mTypePieceIdentite = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSexeSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSexe = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button nextBT = view.findViewById(R.id.next_bt);
        nextBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, BaseActivity.class);
                startActivity(intent);
            }
        });

        Button saveBT = view.findViewById(R.id.save_bt);
        saveBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collectData();
            }
        });

        return view;
    }

    public void initView(View view){
        mNifTI = view.findViewById(R.id.nif_ti);
        mNomTI = view.findViewById(R.id.nom_ti);
        mPostnomTI = view.findViewById(R.id.postnom_ti);
        mMobileOneTI = view.findViewById(R.id.mobile_one_ti);
        mAutreReferenceTI = view.findViewById(R.id.autre_reference_ti);

        mNfiET = view.findViewById(R.id.nif_et);
        mNomET = view.findViewById(R.id.nom_et);
        mPostnomET = view.findViewById(R.id.postnom_et);
        mPrenomET = view.findViewById(R.id.prenom_et);
        dateLabelTV = view.findViewById(R.id.date_label);
        mLieuNaissanceET = view.findViewById(R.id.lieu_naissance_et);
        mMobileOneET = view.findViewById(R.id.mobile_one_et);
        mMobileTwoET = view.findViewById(R.id.mobile_two_et);
        mAdresseElectroniqueET = view.findViewById(R.id.adresse_electronique_et);
        mNumeroPieceidentiteET = view.findViewById(R.id.numero_piece_identite_et);
        mNomPereET = view.findViewById(R.id.nom_et);
        mNomMereET = view.findViewById(R.id.nom_mere_et);
        mAdressePostaleET = view.findViewById(R.id.adresse_postale_et);
        mDetailsAdresseET = view.findViewById(R.id.details_adresse_et);
    }

    public void collectData() {
        mNomTI.setError("");
        mPostnomTI.setError("");
        mMobileOneTI.setError("");
        mAutreReferenceTI.setError("");

        boolean isValid = true;

        mNfi = mNfiET.getText().toString();
        mNom = mNomET.getText().toString();
        mPostnom = mPostnomET.getText().toString();
        mPrenom = mPrenomET.getText().toString();
        mLieuNaissance = mLieuNaissanceET.getText().toString();
        mMobileOne = mMobileOneET.getText().toString();
        mMobileTwo = mMobileTwoET.getText().toString();
        mAdresseElectronique = mAdresseElectroniqueET.getText().toString();
        mNumeroPieceidentite = mNumeroPieceidentiteET.getText().toString();
        mNomPere = mNomPereET.getText().toString();
        mNomMere = mNomMereET.getText().toString();
        mAdressePostale = mAdressePostaleET.getText().toString();
        mDetailsAdresse = mDetailsAdresseET.getText().toString();

        String codeUtilisateur = mSharedPref.getString(Constant.KEY_CODE_UTILISATEUR, null);

        if (mNom.equals("")) {
            mNomTI.setError("Le Nom ne peut pas etre vide");
            isValid = false;
        }
        if (mPostnom.equals("")) {
            mPostnomTI.setError("Le Postnom ne peut pas etre vide");
            isValid = false;
        }
        if (mMobileOne.equals("")) {
            mMobileOneTI.setError("Le numero de telephone ne peut pas etre vide");
            isValid = false;
        }
        if (mDetailsAdresse.equals("")) {
            mAutreReferenceTI.setError("Les details de l'adresse ne peuvent pas etre vide");
            isValid = false;
        }

        if (isValid){

            PersonnePhysique personnePhysique = new PersonnePhysique(mNom, mPostnom, mPrenom, mDateNaissance, mLieuNaissance, mSexe, mTypePieceIdentite, mNumeroPieceidentite, mNomPere, mNomMere, mProfession.getCodeProfession(), mAdressePostale);
            Assujetti assujetti = new Assujetti( mNfi, mMobileOne, mMobileTwo, mAdresseElectronique, mDetailsAdresse, mCellule.getCodeCellule(), codeUtilisateur);

            insertData(personnePhysique, assujetti);
        }

    }

    public void insertData(final PersonnePhysique personnePhysique, final Assujetti assujetti){
        (new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                long[] rowIdAssujetti = DpmerDatabase.getInstance().getIAssujettiDao().insert(assujetti);
                if (rowIdAssujetti.length > 0){
                    personnePhysique.setCodePersonnePhysique(String.valueOf(DpmerDatabase.getInstance().getIAssujettiDao().getLastAssujetti()));
                }
                long[] rowIdPersonnePhysique = DpmerDatabase.getInstance().getIPersonnePhysiqueDao().insert(personnePhysique);
                Log.e(TAG, "collectData: PERSONNE PHYSIQUE ROW ID: " + rowIdPersonnePhysique.length + " " + rowIdPersonnePhysique[0] + " " + rowIdPersonnePhysique );
                Log.e(TAG, "collectData: ASSUJETTI ROW ID: " + rowIdAssujetti.length + " " + rowIdAssujetti[0] + " " + rowIdAssujetti );
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }).execute();
    }

    public void updateLabel(){
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRANCE);

        dateLabelTV.setText(sdf.format(mCalendar.getTime()));
        mDateNaissance = mCalendar.getTime();
    }

    private void populateProfessions(){
        (new AsyncTask<Void, Void, List<Profession>>(){

            @Override
            protected List<Profession> doInBackground(Void... voids) {
                return DpmerDatabase.getInstance().getIProfessionDao().getAllProfessions();
            }

            @Override
            protected void onPostExecute(List<Profession> professions) {
                super.onPostExecute(professions);

                if(professions != null && professions.size() > 0) {
                    professionSpinner.setAdapter(new ArrayAdapter(
                            view.getContext(), android.R.layout.simple_spinner_item,
                            professions));

                    professionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mProfession = (Profession)parent.getItemAtPosition(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        }).execute();
    }

    private void populateCommunes(){
        (new AsyncTask<Void, Void, List<Commune>>(){

            @Override
            protected List<Commune> doInBackground(Void... voids) {
                return DpmerDatabase.getInstance().getICommuneDao().getAllCommunes();
            }

            @Override
            protected void onPostExecute(List<Commune> communes) {
                super.onPostExecute(communes);

                if(communes != null && communes.size() > 0) {
                    communeSpinner.setAdapter(new ArrayAdapter(
                            view.getContext(), android.R.layout.simple_spinner_item,
                            communes));
                }
            }
        }).execute();
    }

    private void populateQuartiers(String selectedCommuneCode){

        if(selectedCommuneCode == null){
            quartierSpinner.setAdapter(null);
            return;
        }

        (new AsyncTask<String, Void, List<Quartier>>(){

            @Override
            protected List<Quartier> doInBackground(String... values) {
                return DpmerDatabase.getInstance().getIQuartierDao().findQuartierByCode(values[0]);
            }

            @Override
            protected void onPostExecute(List<Quartier> quartiers) {
                super.onPostExecute(quartiers);

                if(quartiers != null && quartiers.size() > 0) {
                    quartierSpinner.setAdapter(new ArrayAdapter(
                            view.getContext(), android.R.layout.simple_spinner_item,
                            quartiers));
                }
            }
        }).execute(selectedCommuneCode);
    }

    private void populateCellules(String selectedQuartierCode){

        if(selectedQuartierCode == null){
            celluleSpinner.setAdapter(null);
            return;
        }

        (new AsyncTask<String, Void, List<Cellule>>(){

            @Override
            protected List<Cellule> doInBackground(String... values) {
                return DpmerDatabase.getInstance().getICelluleDao().findCelluleByCode(values[0]);
            }

            @Override
            protected void onPostExecute(List<Cellule> cellules) {
                super.onPostExecute(cellules);

                if(cellules != null && cellules.size() > 0) {
                    celluleSpinner.setAdapter(new ArrayAdapter(
                            view.getContext(), android.R.layout.simple_spinner_item,
                            cellules));
                    celluleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mCellule = (Cellule) parent.getItemAtPosition(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        }).execute(selectedQuartierCode);
    }
}
