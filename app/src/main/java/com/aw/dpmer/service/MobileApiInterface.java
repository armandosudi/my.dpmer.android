package com.aw.dpmer.service;

import com.aw.dpmer.entites.Cellule;
import com.aw.dpmer.entites.Commune;
import com.aw.dpmer.entites.FormeJuridique;
import com.aw.dpmer.entites.NatureActivite;
import com.aw.dpmer.entites.Profession;
import com.aw.dpmer.entites.Profil;
import com.aw.dpmer.entites.Quartier;
import com.aw.dpmer.entites.TypeBatiment;
import com.aw.dpmer.entites.Usage;
import com.aw.dpmer.entites.Utilisateur;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface MobileApiInterface {

    @GET("cellules/")
    public Call<List<Cellule>> getCelluleList();

    @GET("usages/")
    public Call<List<Usage>> getUsageList();

    @GET("naturesactivite/")
    public Call<List<NatureActivite>> getNatureActiviteList();

    @GET("typesbatiment/")
    public Call<List<TypeBatiment>> getTypeBatimentList();

    @GET("communes/")
    public Call<List<Commune>> getCommuneList();

    @GET("quartiers/")
    public Call<List<Quartier>> getQuartierList();

    @GET("profils/")
    public Call<List<Profil>> getProfilList();

    @GET("professions/")
    public Call<List<Profession>> getProfessionList();

    @GET("formesjuridiques/")
    public Call<List<FormeJuridique>> getFormeJuridiqueList();

    @GET("authentify/{username}/{password}")
    public Call<Utilisateur> getUtilisateur(@Path("username") String username, @Path("password") String  password );

    @Headers({"Content-Type: application/json"})
    @POST("utilisateurinfo/")
    public Call<Utilisateur> getUtilisateur(@Body Utilisateur utilisateur);

}
