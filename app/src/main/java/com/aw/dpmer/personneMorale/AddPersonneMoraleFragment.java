package com.aw.dpmer.personneMorale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.aw.dpmer.activites.BaseActivity;
import com.aw.dpmer.R;
import com.aw.dpmer.activites.BaseAddActivity;
import com.aw.dpmer.database.DpmerDatabase;
import com.aw.dpmer.entites.Assujetti;
import com.aw.dpmer.entites.Cellule;
import com.aw.dpmer.entites.Commune;
import com.aw.dpmer.entites.FormeJuridique;
import com.aw.dpmer.entites.PersonneMorale;
import com.aw.dpmer.entites.PersonnePhysique;
import com.aw.dpmer.entites.Quartier;
import com.aw.dpmer.utils.Constant;

import java.text.Normalizer;
import java.util.List;

import static android.content.ContentValues.TAG;

public class AddPersonneMoraleFragment extends Fragment {

    private Activity mActivity;
    private View view;
    private Spinner formeJuridiqueSpinner, communeSpinner, quartierSpinner, celluleSpinner;
    private TextInputLayout nifTI, raisonSocialeTI, sigleTI, mobileOneTI;
    private EditText mNfiET, mRaisonSocialeET, mSigleET, mEnseigneET, mMobileOneET, mMobileTwoET, mAdresseElectroniqueET, mDetailsAdresseET;
    private String mNfi, mRaisonSociale, mSigle, mEnseigne, mMobileOne, mMobileTwo, mAdresseElectronique, mDetailsAdresse;

    private FormeJuridique mFormeJuridique;
    private Cellule mCellule;
    private Quartier mQuartier;
    private Commune mCommune;

    SharedPreferences mSharedPref;

    public AddPersonneMoraleFragment() {

    }

    public static AddPersonneMoraleFragment newInstance() {
        AddPersonneMoraleFragment fragment = new AddPersonneMoraleFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActivity = getActivity();
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_add_personne_morale, container, false);

        initView(view);

        formeJuridiqueSpinner = view.findViewById(R.id.forme_jurdique_sp);
        communeSpinner = view.findViewById(R.id.commune_sp);
        quartierSpinner = view.findViewById(R.id.quartier_sp);
        celluleSpinner = view.findViewById(R.id.cellule_sp);

        populateFormesJuridiques();
        populateCommunes();

        communeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCommune = (Commune)parent.getItemAtPosition(position);
                populateQuartiers(mCommune.codeCommune);
                populateCellules(null);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        quartierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mQuartier = (Quartier)parent.getItemAtPosition(position);
                populateCellules(mQuartier.codeQuartier);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Button button = view.findViewById(R.id.button_activites);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, BaseActivity.class);
                startActivity(intent);
            }
        });

        Button enregistrerBT = view.findViewById(R.id.enregistrer_bt);
        enregistrerBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collectData();
            }
        });

        return view;
    }

    public void initView(View view){
        nifTI = view.findViewById(R.id.nif_ti);
        raisonSocialeTI = view.findViewById(R.id.raison_sociale_ti);
        sigleTI = view.findViewById(R.id.sigle_ti);
        mobileOneTI = view.findViewById(R.id.mobile_one_ti);

        mNfiET = view.findViewById(R.id.nif_et);
        mRaisonSocialeET = view.findViewById(R.id.raison_sociale_et);
        mSigleET = view.findViewById(R.id.sigle_et);
        mEnseigneET = view.findViewById(R.id.enseigne_et);
        mMobileOneET = view.findViewById(R.id.mobile_one_et);
        mMobileTwoET = view.findViewById(R.id.mobile_two_et);
        mAdresseElectroniqueET = view.findViewById(R.id.adresse_electronique_et);
        mDetailsAdresseET = view.findViewById(R.id.details_adresse_et);

    }

    public void collectData(){

        nifTI.setError("");
        raisonSocialeTI.setError("");
        sigleTI.setError("");
        mobileOneTI.setError("");

        boolean isValid = true;

        mNfi = mNfiET.getText().toString();
        mRaisonSociale = mRaisonSocialeET.getText().toString();
        mSigle = mSigleET.getText().toString();
        mEnseigne = mEnseigneET.getText().toString();
        mMobileOne = mMobileOneET.getText().toString();
        mMobileTwo = mMobileTwoET.getText().toString();
        mAdresseElectronique = mAdresseElectroniqueET.getText().toString();
        mDetailsAdresse = mDetailsAdresseET.getText().toString();

        String codeUtilisateur = mSharedPref.getString(Constant.KEY_CODE_UTILISATEUR, null);

        if (mNfi.equals("")) {
            nifTI.setError("NIF ne peut pas etre vide");
            isValid = false;
        }
        if (mRaisonSociale.equals("")) {
            raisonSocialeTI.setError("Raison sociale ne peut pas etre vide");
            isValid = false;
        }
        if (mMobileOne.equals("")) {
            mobileOneTI.setError("Le numero de telephone ne peut pas etre vide");
            isValid = false;
        }
        if (mSigle.equals("")) {
            sigleTI.setError("Le sigle ne peut pas etre vide");
            isValid = false;
        }

        if (isValid) {
            PersonneMorale personneMorale = new PersonneMorale( mRaisonSociale, mFormeJuridique.getCodeFormeJuridique(), mEnseigne, mSigle);
            Assujetti assujetti = new Assujetti( mNfi, mMobileOne, mMobileTwo, mAdresseElectronique, mDetailsAdresse, mCellule.getCodeCellule(), codeUtilisateur );

            insertData(personneMorale, assujetti);
        }

    }

    public void insertData(final PersonneMorale personneMorale, final Assujetti assujetti){
        (new AsyncTask<Void, Void, Integer>(){
            @Override
            protected Integer doInBackground(Void... voids) {
                long[] rowIdAssujetti = DpmerDatabase.getInstance().getIAssujettiDao().insert(assujetti);
                int codeAssujetti = 0;

                if (rowIdAssujetti.length > 0){
                    codeAssujetti = DpmerDatabase.getInstance().getIAssujettiDao().getLastAssujetti();
                    Log.e(TAG, "doInBackground: CODE ASSUJETTI :" + codeAssujetti );
                    personneMorale.setCodePersonneMorale(String.valueOf(codeAssujetti));

                }

                long[] rowIdPersonneMorale = DpmerDatabase.getInstance().getIPersonneMoraleDao().insert(personneMorale);
                Log.e(TAG, "collectData: PERSONNE MORALE ROW ID: " + rowIdPersonneMorale.length + " " + rowIdPersonneMorale[0] + " " + rowIdPersonneMorale );
                Log.e(TAG, "collectData: ASSUJETTI ROW ID: " + rowIdAssujetti.length + " " + rowIdAssujetti[0] + " " + rowIdAssujetti );
                return codeAssujetti;
            }

            @Override
            protected void onPostExecute(Integer codeAssujetti) {
                super.onPostExecute(codeAssujetti);

                if (codeAssujetti != 0){
                    Intent intent = new Intent(mActivity, BaseActivity.class);
                    intent.putExtra(Constant.KEY_CODE_ASSUJETTI, codeAssujetti);
                   startActivity(intent);
                }

            }
        }).execute();
    }

    private void populateFormesJuridiques(){
        (new AsyncTask<Void, Void, List<FormeJuridique>>(){

            @Override
            protected List<FormeJuridique> doInBackground(Void... voids) {
                return DpmerDatabase.getInstance().getIFormeJuridiqueDao().getAllFormesJuridiques();
            }

            @Override
            protected void onPostExecute(List<FormeJuridique> formeJuridiques) {
                super.onPostExecute(formeJuridiques);

                if(formeJuridiques != null && formeJuridiques.size() > 0) {
                    formeJuridiqueSpinner.setAdapter(new ArrayAdapter(
                            view.getContext(), android.R.layout.simple_spinner_item,
                            formeJuridiques));
                    formeJuridiqueSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mFormeJuridique = (FormeJuridique) parent.getItemAtPosition(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        }).execute();
    }

    private void populateCommunes(){
        (new AsyncTask<Void, Void, List<Commune>>(){

            @Override
            protected List<Commune> doInBackground(Void... voids) {
                return DpmerDatabase.getInstance().getICommuneDao().getAllCommunes();
            }

            @Override
            protected void onPostExecute(List<Commune> communes) {
                super.onPostExecute(communes);

                if(communes != null && communes.size() > 0) {
                    communeSpinner.setAdapter(new ArrayAdapter(
                            view.getContext(), android.R.layout.simple_spinner_item,
                            communes));
                }
            }
        }).execute();
    }

    private void populateQuartiers(String selectedCommuneCode){

        if(selectedCommuneCode == null){
            quartierSpinner.setAdapter(null);
            return;
        }

        (new AsyncTask<String, Void, List<Quartier>>(){

            @Override
            protected List<Quartier> doInBackground(String... values) {
                return DpmerDatabase.getInstance().getIQuartierDao().findQuartierByCode(values[0]);
            }

            @Override
            protected void onPostExecute(List<Quartier> quartiers) {
                super.onPostExecute(quartiers);

                if(quartiers != null && quartiers.size() > 0) {
                    quartierSpinner.setAdapter(new ArrayAdapter(
                            view.getContext(), android.R.layout.simple_spinner_item,
                            quartiers));
                }
            }
        }).execute(selectedCommuneCode);
    }

    private void populateCellules(String selectedQuartierCode){

        if(selectedQuartierCode == null){
            celluleSpinner.setAdapter(null);
            return;
        }

        (new AsyncTask<String, Void, List<Cellule>>(){

            @Override
            protected List<Cellule> doInBackground(String... values) {
                return DpmerDatabase.getInstance().getICelluleDao().findCelluleByCode(values[0]);
            }

            @Override
            protected void onPostExecute(List<Cellule> cellules) {
                super.onPostExecute(cellules);

                if(cellules != null && cellules.size() > 0) {
                    celluleSpinner.setAdapter(new ArrayAdapter(
                            view.getContext(), android.R.layout.simple_spinner_item,
                            cellules));
                    celluleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mCellule = (Cellule) parent.getItemAtPosition(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        }).execute(selectedQuartierCode);
    }
}
