package com.aw.dpmer.dashboard;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aw.dpmer.R;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment {

    List<DashboardItem> mDashboardItems = new ArrayList<>();
    Context mContext;

    public DashboardFragment() {
    }

    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getActivity();

        mDashboardItems.add(new DashboardItem("Personnes Physiques", "21", "#FF0082D1"));
        mDashboardItems.add(new DashboardItem("Personnes Morales", "50", "#FFDC4B39"));
        mDashboardItems.add(new DashboardItem("Proprietes", "199", "#FFF29B12"));
        mDashboardItems.add(new DashboardItem("Apparetement", "60", "#FF0DCAF9"));
        mDashboardItems.add(new DashboardItem("Declarations", "31", "#FFD71B60"));
        mDashboardItems.add(new DashboardItem("Montants", "1000", "#FF00A55A"));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        RecyclerView dashboardRV = view.findViewById(R.id.dashboard_rv);
        DashboardAdapter dashboardAdapter = new DashboardAdapter(mContext, mDashboardItems);

        dashboardRV.setAdapter(dashboardAdapter);
        dashboardRV.setLayoutManager(new GridLayoutManager(mContext, 2));
        return view;
    }
}
