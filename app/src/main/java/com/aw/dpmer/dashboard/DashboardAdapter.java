package com.aw.dpmer.dashboard;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aw.dpmer.R;

import java.util.List;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.VH> {

    Context mContext;
    List<DashboardItem> mDashboardItems;

    static class VH extends RecyclerView.ViewHolder {
        ImageView iconIV;
        TextView numberTV;
        TextView titleTV;

        public VH(View view) {
            super(view);
            iconIV = view.findViewById(R.id.icon_iv);
            numberTV = view.findViewById(R.id.number_tv);
            titleTV = view.findViewById(R.id.title_tv);
        }
    }

    public DashboardAdapter(Context context, List<DashboardItem> dashboarditems) {
        mContext = context;
        mDashboardItems = dashboarditems;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View dashboardItemView = LayoutInflater.from(mContext).
                inflate(R.layout.dashboard_recycler_item, parent, false);

        return new VH(dashboardItemView);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {

        final DashboardItem dashboardItem = mDashboardItems.get(position);

        holder.titleTV.setText(dashboardItem.getTitle());
        holder.numberTV.setText(dashboardItem.getNumber());
        holder.itemView.setBackgroundColor(Color.parseColor(dashboardItem.getBackgroundColor()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(mContext, "" + dashboardItem.getTitle(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDashboardItems.size();
    }
}
