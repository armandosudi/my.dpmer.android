package com.aw.dpmer.propriete;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.aw.dpmer.R;
import com.aw.dpmer.database.DpmerDatabase;
import com.aw.dpmer.entites.Cellule;
import com.aw.dpmer.entites.Commune;
import com.aw.dpmer.entites.Propriete;
import com.aw.dpmer.entites.Quartier;
import com.aw.dpmer.entites.TypeBatiment;
import com.aw.dpmer.entites.Usage;
import com.aw.dpmer.utils.Constant;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ProprieteAddFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProprieteAddFragment extends Fragment {

    SharedPreferences mSharedPref;

    private Activity mActivity;
    private Spinner  natureProprieteSP, communeSP, quartierSP, celluleSP, typeBatimentSP, compteurEauSP, compteurElectriciteSP, usageSP;
    private EditText mReferenceET, detailAdresseET, natureTaxeET, mNombreEtageET, mSuperficieET;

    private String mReference, mDetailAdresse, mNatureTaxe, mNaturePropriete, mCompteurEau, mCompteurElectricite;
    private int mNombreEtage;
    private float mSuperficie;

    private Cellule mCellule;
    private Commune mCommune;
    private Quartier mQuartier;
    private TypeBatiment mTypeBatiment;
    private Usage mUsage;

    private int mCodeAssujetti;

    private Calendar mCalendar = Calendar.getInstance();

    public ProprieteAddFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProprieteAddFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProprieteAddFragment newInstance(int codeAssujetti) {
        ProprieteAddFragment fragment = new ProprieteAddFragment();
        Bundle args = new Bundle();
        args.putInt(Constant.KEY_CODE_ASSUJETTI, codeAssujetti);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        mCodeAssujetti = args.getInt(Constant.KEY_CODE_ASSUJETTI);

        mActivity = getActivity();
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
//        mSharedPref = mActivity.getPreferences(Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_propriete_add, container, false);

        initView(view);

        populateCommunes();
        populateUsage();
        populateTypeBatiment();

        Button saveBt = view.findViewById(R.id.save_bt);
        saveBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                collectData();
            }
        });

        return view;
    }

    public void initView(View view){
        natureProprieteSP = view.findViewById(R.id.nature_propriete_sp);
        communeSP = view.findViewById(R.id.commune_sp);
        quartierSP = view.findViewById(R.id.quartier_sp);
        celluleSP = view.findViewById(R.id.cellule_sp);
        typeBatimentSP = view.findViewById(R.id.type_batiment_sp);
        compteurEauSP = view.findViewById(R.id.compteur_eau_sp);
        compteurElectriciteSP = view.findViewById(R.id.compteur_electricite_sp);;
        usageSP = view.findViewById(R.id.usage_sp);

        mReferenceET = view.findViewById(R.id.reference_et);
        detailAdresseET = view.findViewById(R.id.details_adresse_et);
        natureTaxeET = view.findViewById(R.id.nature_taxe_et);
        mNombreEtageET = view.findViewById(R.id.etages_et);
        mSuperficieET = view.findViewById(R.id.superficie_et);

        communeSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCommune = (Commune)parent.getItemAtPosition(position);
                populateQuartiers(mCommune.codeCommune);
                populateCellules(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        quartierSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mQuartier = (Quartier)parent.getItemAtPosition(position);
                populateCellules(mQuartier.codeQuartier);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        natureProprieteSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mNaturePropriete = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        typeBatimentSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mTypeBatiment = (TypeBatiment) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        compteurEauSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCompteurEau = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        compteurElectriciteSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCompteurElectricite = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        usageSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mUsage = (Usage) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void populateCommunes(){
        (new AsyncTask<Void, Void, List<Commune>>(){

            @Override
            protected List<Commune> doInBackground(Void... voids) {
                return DpmerDatabase.getInstance().getICommuneDao().getAllCommunes();
            }

            @Override
            protected void onPostExecute(List<Commune> communes) {
                super.onPostExecute(communes);

                if(communes != null && communes.size() > 0) {
                    communeSP.setAdapter(new ArrayAdapter(
                            mActivity, android.R.layout.simple_spinner_item,
                            communes));
                }
            }
        }).execute();
    }

    private void populateQuartiers(String selectedCommuneCode){

        if(selectedCommuneCode == null){
            quartierSP.setAdapter(null);
            return;
        }

        (new AsyncTask<String, Void, List<Quartier>>(){

            @Override
            protected List<Quartier> doInBackground(String... values) {
                return DpmerDatabase.getInstance().getIQuartierDao().findQuartierByCode(values[0]);
            }

            @Override
            protected void onPostExecute(List<Quartier> quartiers) {
                super.onPostExecute(quartiers);

                if(quartiers != null && quartiers.size() > 0) {
                    quartierSP.setAdapter(new ArrayAdapter(
                            mActivity, android.R.layout.simple_spinner_item,
                            quartiers));
                }
            }
        }).execute(selectedCommuneCode);
    }

    private void populateCellules(String selectedQuartierCode){

        if(selectedQuartierCode == null){
            celluleSP.setAdapter(null);
            return;
        }

        (new AsyncTask<String, Void, List<Cellule>>(){

            @Override
            protected List<Cellule> doInBackground(String... values) {
                return DpmerDatabase.getInstance().getICelluleDao().findCelluleByCode(values[0]);
            }

            @Override
            protected void onPostExecute(List<Cellule> cellules) {
                super.onPostExecute(cellules);

                if(cellules != null && cellules.size() > 0) {
                    celluleSP.setAdapter(new ArrayAdapter(
                            mActivity, android.R.layout.simple_spinner_item,
                            cellules));
                    celluleSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mCellule = (Cellule) parent.getItemAtPosition(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        }).execute(selectedQuartierCode);
    }

    private void populateUsage() {
        (new AsyncTask<Void, Void, List<Usage>>(){

            @Override
            protected List<Usage> doInBackground(Void... voids) {
                return DpmerDatabase.getInstance().getIUsageDao().getAllUsageList();
            }

            @Override
            protected void onPostExecute(List<Usage> usages) {
                super.onPostExecute(usages);

                if(usages != null && usages.size() > 0) {
                    usageSP.setAdapter(new ArrayAdapter(
                            mActivity, android.R.layout.simple_spinner_item,
                            usages));
                }
            }
        }).execute();
    }

    private void populateTypeBatiment() {
        (new AsyncTask<Void, Void, List<TypeBatiment>>(){

            @Override
            protected List<TypeBatiment> doInBackground(Void... voids) {
                return DpmerDatabase.getInstance().getITypeBatimentDao().getAllTypeBatimentList();
            }

            @Override
            protected void onPostExecute(List<TypeBatiment> typeBatiments) {
                super.onPostExecute(typeBatiments);

                if(typeBatiments != null && typeBatiments.size() > 0) {
                    typeBatimentSP.setAdapter(new ArrayAdapter(
                            mActivity, android.R.layout.simple_spinner_item,
                            typeBatiments));
                }
            }
        }).execute();
    }

    private void collectData() {

        mReference = mReferenceET.getText().toString();
        mDetailAdresse = detailAdresseET.getText().toString();
        mNatureTaxe = natureTaxeET.getText().toString();

        mNombreEtage = Integer.parseInt(mNombreEtageET.getText().toString());
        mSuperficie = Float.parseFloat(mSuperficieET.getText().toString());

        String codeUtilisateur = mSharedPref.getString(Constant.KEY_CODE_UTILISATEUR, "000");

        Date dateEnregistrement = mCalendar.getTime();
        //TODO Validation des valeurs
        //TODO generer le code de propriete
        //TODO recuperer le code assujetti aussi
        Propriete propriete = new Propriete(mReference, mNaturePropriete, mSuperficie, mDetailAdresse, mCellule.getCodeCellule(), mTypeBatiment.getCodeTypeBatiment(), mCodeAssujetti, mNombreEtage, mUsage.getCodeUsage(), mNatureTaxe, mCompteurEau, mCompteurElectricite, "url_photo_one", "url_photo_two", "url_photo_three", "url_photo_four", 10, 10, dateEnregistrement.toString(), codeUtilisateur );
        Log.e(TAG, "collectData: " + propriete );

        insertData(propriete);
    }

    public void insertData(final Propriete propriete){
        (new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(Void... voids) {
                long[] rowIdPropriete = DpmerDatabase.getInstance().getIProprieteDao().insert(propriete);

                Log.e(TAG, "collectData: PROPRIETE ROW ID: " + rowIdPropriete.length + " " + rowIdPropriete[0] + " ");
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }).execute();
    }
}
