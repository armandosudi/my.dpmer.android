package com.aw.dpmer.propriete;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.aw.dpmer.R;
import com.aw.dpmer.adapters.ProprieteAdapter;
import com.aw.dpmer.database.DpmerDatabase;
import com.aw.dpmer.entites.PersonneMorale;
import com.aw.dpmer.entites.Propriete;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * Use the {@link ProprieteListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProprieteListFragment extends Fragment {

    List<Propriete> mProprieteList = new ArrayList<>();
    Activity mActivity;
    ProprieteAdapter mProprieteAdapter;

    public ProprieteListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProprieteListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProprieteListFragment newInstance() {
        ProprieteListFragment fragment = new ProprieteListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_propriete_list, container, false);

        mProprieteAdapter = new ProprieteAdapter(mActivity, mProprieteList);
        RecyclerView proprieteRV = view.findViewById(R.id.propriete_rv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, LinearLayout.VERTICAL, false);
        proprieteRV.setLayoutManager(linearLayoutManager);
        proprieteRV.setHasFixedSize(true);
        proprieteRV.addItemDecoration(new DividerItemDecoration(mActivity, linearLayoutManager.getOrientation()));
        proprieteRV.setAdapter(mProprieteAdapter);

        findAllPropriete();

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    public void findAllPropriete(){
        (new AsyncTask<Void, Void, List<Propriete>>(){
            @Override
            protected List<Propriete>doInBackground(Void... voids) {
                List<Propriete> proprieteList = DpmerDatabase.getInstance().getIProprieteDao().getAllProprieteList();
                Log.e(TAG, "doInBackground: propriete list size : " + proprieteList.size() );
                return proprieteList;
            }

            @Override
            protected void onPostExecute(List<Propriete> proprietes) {
                mProprieteAdapter.addProprieteList(proprietes);
                mProprieteAdapter.notifyDataSetChanged();
            }
        }).execute();
    }

}
