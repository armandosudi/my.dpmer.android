package com.aw.dpmer.database;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.*;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.aw.dpmer.dao.IActiviteDao;
import com.aw.dpmer.dao.ICelluleDao;
import com.aw.dpmer.dao.ICommuneDao;
import com.aw.dpmer.dao.IExerciceFiscaleDao;
import com.aw.dpmer.dao.IFormeJuridiqueDao;
import com.aw.dpmer.dao.INatureActiviteDao;
import com.aw.dpmer.dao.IPersonneMoraleDao;
import com.aw.dpmer.dao.IPersonnePhysiqueDao;
import com.aw.dpmer.dao.IProfessionDao;
import com.aw.dpmer.dao.IProfilDao;
import com.aw.dpmer.dao.IProprieteDao;
import com.aw.dpmer.dao.IProprieteLocataireDao;
import com.aw.dpmer.dao.IQuartierDao;
import com.aw.dpmer.dao.IRangDao;
import com.aw.dpmer.dao.ISuccursaleDao;
import com.aw.dpmer.dao.ITauxDao;
import com.aw.dpmer.dao.ITypeBatimentDao;
import com.aw.dpmer.dao.IUsageDao;
import com.aw.dpmer.dao.IUtilisateurDao;
import com.aw.dpmer.entites.*;
import com.aw.dpmer.dao.IAssujettiDao;
import com.aw.dpmer.utils.DateConverts;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Database(entities = {
        Assujetti.class,
        Cellule.class,
        Commune.class,
        FormeJuridique.class,
        NatureActivite.class,
        PersonneMorale.class,
        PersonnePhysique.class,
        Profil.class,
        Propriete.class,
        Quartier.class,
        Rang.class,
        TypeBatiment.class,
        Usage.class,
        Utilisateur.class,
        Activite.class,
        Declaration.class,
        DeclaratpionPropriete.class,
        ExerciceFiscale.class,
        Profession.class,
        ProprieteLocataire.class,
        Succursale.class,
        Taux.class,},
        version = 10, exportSchema = false)
@TypeConverters(DateConverts.class)
public abstract class DpmerDatabase extends RoomDatabase {

    private static final String DB_NAME = "dpmer.db";

    public abstract IAssujettiDao getIAssujettiDao();

    public abstract IActiviteDao getIActiviteDao();

    public abstract ICelluleDao getICelluleDao();

    public abstract ICommuneDao getICommuneDao();

    public abstract IExerciceFiscaleDao getIExerciceFiscaleDao();

    public abstract IFormeJuridiqueDao getIFormeJuridiqueDao();

    public abstract INatureActiviteDao getINatureActiviteDao();

    public abstract IPersonneMoraleDao getIPersonneMoraleDao();

    public abstract IPersonnePhysiqueDao getIPersonnePhysiqueDao();

    public abstract IProfessionDao getIProfessionDao();

    public abstract IProfilDao getIProfilDao();

    public abstract IProprieteDao getIProprieteDao();

    public abstract IProprieteLocataireDao getIProprieteLocataireDao();

    public abstract IQuartierDao getIQuartierDao();

    public abstract IRangDao getIRangDao();

    public abstract ISuccursaleDao getISuccursaleDao();

    public abstract ITauxDao getITauxDao();

    public abstract ITypeBatimentDao getITypeBatimentDao();

    public abstract IUsageDao getIUsageDao();

    public abstract IUtilisateurDao getIUtilisateuDao();

    private static DpmerDatabase INSTANCE;

    public static DpmerDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (DpmerDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            DpmerDatabase.class, DB_NAME)
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    static public DpmerDatabase getInstance() {
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private IAssujettiDao IAssujettiDao;
        private IPersonneMoraleDao IPersonneMoraleDao;
        private IPersonnePhysiqueDao IPersonnePhysiqueDao;
        private IFormeJuridiqueDao IFormeJuridiqueDao;

        PopulateDbAsync(DpmerDatabase db) {
            IAssujettiDao = db.getIAssujettiDao();
            IPersonneMoraleDao = db.getIPersonneMoraleDao();
            IPersonnePhysiqueDao = db.getIPersonnePhysiqueDao();
            IFormeJuridiqueDao = db.getIFormeJuridiqueDao();
        }

        @Override
        protected Void doInBackground(final Void... params) {


//            IAssujettiDao.deleteAll();
//            Assujetti ass1 = new Assujetti("5000","100001","0816817522","099999999","johnolenga@gmail.com", "9,Jardin, Limete, Socopao1", "101");
//            IAssujettiDao.insert(ass1);
//            Assujetti ass2 = new Assujetti("5001","100002","0888888888","077777777","johnolenga@gmail.com", "9,Jardin, Limete, Socopao1", "101");
//            IAssujettiDao.insert(ass2);
//            Assujetti ass3 = new Assujetti("5002","100003","066666666","055555555","johnolenga@gmail.com", "9,Jardin, Limete, Socopao1", "101");
//            IAssujettiDao.insert(ass3);
//
//            IFormeJuridiqueDao.deleteAll();
//            FormeJuridique f1 = new FormeJuridique("51","Societe","SARL");
//            IFormeJuridiqueDao.insert(f1);
//
//            IPersonneMoraleDao.deleteAll();
//            PersonneMorale p1 = new PersonneMorale("5000","VODACOM","VODACOM","17","0167NAT");
//            IPersonneMoraleDao.insert(p1);
//            PersonneMorale p2 = new PersonneMorale("5001","AFRICELL","AFRICELL","18","0168NAT");
//            IPersonneMoraleDao.insert(p2);
//            PersonneMorale p3 = new PersonneMorale("5002","AIRTEL","AIRTEL","19","0169NAT");
//            IPersonneMoraleDao.insert(p3);
            return null;
        }
    }
}
