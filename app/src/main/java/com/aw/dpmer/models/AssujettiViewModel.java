package com.aw.dpmer.models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import com.aw.dpmer.entites.Assujetti;

import java.util.List;

/**
 * Created by jean.olenga on 19/03/2018.
 */

public class AssujettiViewModel extends AndroidViewModel {

    private LiveData<List<Assujetti>> myListAssujetti;

    public AssujettiViewModel(Application application) {
        super(application);
        /*myRepos = new AssujettiRepository(application);
        myListAssujetti = myRepos.getAllAssujetti();*/
    }

    public LiveData<List<Assujetti>> getAllAssujetti(){return myListAssujetti;}

    public void insert (Assujetti assujetti){
        //myRepos.insert(assujetti);

    }
}
