package com.aw.dpmer.models;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import com.aw.dpmer.entites.PersonneMorale;

import java.util.List;

/**
 * Created by jean.olenga on 22/03/2018.
 */

public class PersonneMoraleViewModel extends AndroidViewModel {
    //private PersonneMoraleRepository myRepos;
    private LiveData<List<PersonneMorale>> myListPersonneMorale;

    public PersonneMoraleViewModel(Application application) {
        super(application);
        //myRepos = new PersonneMoraleRepository(application);
        //myListPersonneMorale = myRepos.getAllPersonneMorale();
    }
    public LiveData<List<PersonneMorale>> getAllPersonneMorale(){return myListPersonneMorale;};
    public void insert (PersonneMorale personneMorale){
        //myRepos.insert(personneMorale);

    }
}

