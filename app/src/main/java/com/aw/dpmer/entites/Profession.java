package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sugar
 */

@Entity(tableName = "PROFESSION")
public class Profession {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_PROFESSION")
    @SerializedName("CodeProfession")
    public String codeProfession;
    @NonNull
    @ColumnInfo(name = "LIBELLE")
    @SerializedName("Libelle")
    public String libelle;

    public Profession(@NonNull String codeProfession, @NonNull String libelle) {
        this.codeProfession = codeProfession;
        this.libelle = libelle;
    }

    @NonNull
    public String getCodeProfession() {
        return codeProfession;
    }

    public void setCodeProfession(@NonNull String codeProfession) {
        this.codeProfession = codeProfession;
    }

    @NonNull
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(@NonNull String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return libelle;
    }
}
