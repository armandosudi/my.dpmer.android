package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "PROFIL")
public class Profil {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_PROFIL")
    @SerializedName("CodeProfil")
    public int codeProfil;
    @ColumnInfo(name = "LIBELLE")
    @SerializedName("Libelle")
    public String libelle;

    @NonNull
    public int getCodeProfil() {
        return codeProfil;
    }

    public void setCodeProfil(@NonNull int codeProfil) {
        this.codeProfil = codeProfil;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Profil(@NonNull int codeProfil, String libelle) {
        this.codeProfil = codeProfil;
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return "Profil{" +
                "codeProfil=" + codeProfil +
                ", libelle='" + libelle + '\'' +
                '}';
    }
}
