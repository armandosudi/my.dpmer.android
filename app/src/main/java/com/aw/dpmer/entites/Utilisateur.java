package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "UTILISATEUR",foreignKeys =
        {@ForeignKey(entity = Profil.class,parentColumns = "CODE_PROFIL",childColumns = "CODE_PROFIL")})
public class Utilisateur {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_UTILISATEUR")
    @SerializedName("CodeUtilisateur")
    public String codeUtilisateur;
    @NonNull
    @ColumnInfo(name = "NOM_COMPLET")
    @SerializedName("NomComplet")
    public String nomComplet;
    @NonNull
    @ColumnInfo(name = "EMAIL")
    @SerializedName("Email")
    public String email;
    @NonNull
    @ColumnInfo(name = "PASSWORD")
    @SerializedName("Password")
    public String password;
    @NonNull
    @ColumnInfo(name = "ACTIF")
    @SerializedName("Actif")
    public Boolean actif;
    @NonNull
    @ColumnInfo(name = "CODE_PROFIL")
    @SerializedName("CodeProfil")
    public int codeProfil;
    @NonNull
    @ColumnInfo(name = "MAC_ADDRESS")
    @SerializedName("MacAddress")
    public String macAddress;

//    @Ignore
//    public Profil profil;

    public Utilisateur(@NonNull String codeUtilisateur, @NonNull String nomComplet, @NonNull String email, @NonNull String password, @NonNull Boolean actif, @NonNull int codeProfil, @NonNull String macAddress) {
        this.codeUtilisateur = codeUtilisateur;
        this.nomComplet = nomComplet;
        this.email = email;
        this.password = password;
        this.actif = actif;
        this.codeProfil = codeProfil;
        this.macAddress = macAddress;
    }

    @NonNull
    public String getCodeUtilisateur() {
        return codeUtilisateur;
    }

    public void setCodeUtilisateur(@NonNull String codeUtilisateur) {
        this.codeUtilisateur = codeUtilisateur;
    }

    @NonNull
    public String getNomComplet() {
        return nomComplet;
    }

    public void setNomComplet(@NonNull String nomComplet) {
        this.nomComplet = nomComplet;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NonNull String email) {
        this.email = email;
    }

    @NonNull
    public String getPassword() {
        return password;
    }

    public void setPassword(@NonNull String password) {
        this.password = password;
    }

    @NonNull
    public Boolean getActif() {
        return actif;
    }

    public void setActif(@NonNull Boolean actif) {
        this.actif = actif;
    }

    @NonNull
    public int getCodeProfil() {
        return codeProfil;
    }

    public void setCodeProfil(@NonNull int codeProfil) {
        this.codeProfil = codeProfil;
    }

    @NonNull
    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(@NonNull String macAddress) {
        this.macAddress = macAddress;
    }
}
