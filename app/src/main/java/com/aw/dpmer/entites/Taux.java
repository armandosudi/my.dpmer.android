package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created By Sugar
 */

@Entity(tableName = "TAUX", foreignKeys = {
        @ForeignKey(entity = Rang.class, parentColumns = "CODE_RANG", childColumns = "CODE_RANG"),
        @ForeignKey(entity = TypeBatiment.class, parentColumns = "CODE_TYPE_BATIMENT", childColumns = "CODE_TYPE_BATIMENT")
})
public class Taux {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_TAUX")
    public String codeTaux;
    @NonNull
    @ColumnInfo(name = "CODE_RANG")
    public String codeRang;
    @NonNull
    @ColumnInfo(name = "CODE_TYPE_BATIMENT")
    public String codeTypeBatiment;
    @NonNull
    @ColumnInfo(name = "VALEUR")
    public float valeur;
    @NonNull
    @ColumnInfo(name = "STATUT")
    public String statut;
    @NonNull
    @ColumnInfo(name = "MODE_CALCUL")
    public String modeCalcul;

    public Taux(@NonNull String codeTaux, @NonNull String codeRang, @NonNull String codeTypeBatiment, @NonNull float valeur, @NonNull String statut, @NonNull String modeCalcul) {
        this.codeTaux = codeTaux;
        this.codeRang = codeRang;
        this.codeTypeBatiment = codeTypeBatiment;
        this.valeur = valeur;
        this.statut = statut;
        this.modeCalcul = modeCalcul;
    }

    @NonNull
    public String getCodeTaux() {
        return codeTaux;
    }

    public void setCodeTaux(@NonNull String codeTaux) {
        this.codeTaux = codeTaux;
    }

    @NonNull
    public String getCodeRang() {
        return codeRang;
    }

    public void setCodeRang(@NonNull String codeRang) {
        this.codeRang = codeRang;
    }

    @NonNull
    public String getCodeTypeBatiment() {
        return codeTypeBatiment;
    }

    public void setCodeTypeBatiment(@NonNull String codeTypeBatiment) {
        this.codeTypeBatiment = codeTypeBatiment;
    }

    @NonNull
    public float getValeur() {
        return valeur;
    }

    public void setValeur(@NonNull float valeur) {
        this.valeur = valeur;
    }

    @NonNull
    public String getStatut() {
        return statut;
    }

    public void setStatut(@NonNull String statut) {
        this.statut = statut;
    }

    @NonNull
    public String getModeCalcul() {
        return modeCalcul;
    }

    public void setModeCalcul(@NonNull String modeCalcul) {
        this.modeCalcul = modeCalcul;
    }
}
