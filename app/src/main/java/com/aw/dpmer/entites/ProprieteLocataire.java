package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

@Entity(tableName = "PROPRIETE_LOCATAIRE", foreignKeys = {
        @ForeignKey(entity = Propriete.class, parentColumns = "CODE_PROPRIETE", childColumns = "CODE_PROPRIETE")
})
public class ProprieteLocataire {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_PROPRIETE_LOCATAIRE")
    public String codeProprieteLocataire;
    @NonNull
    @ColumnInfo(name = "CODE_PROPRIETE")
    public String codePropriete;
    @NonNull
    @ColumnInfo(name = "DATE_SIGNATURE_CONTRAT")
    public Date dateSignatureContrat;
    @NonNull
    @ColumnInfo(name = "NOM")
    public String nom;
    @NonNull
    @ColumnInfo(name = "POSTNOM")
    public String postNom;
    @NonNull
    @ColumnInfo(name = "PRENOM")
    public String prenom;

    public ProprieteLocataire(@NonNull String codeProprieteLocataire, @NonNull String codePropriete, @NonNull Date dateSignatureContrat, @NonNull String nom, @NonNull String postNom, @NonNull String prenom) {
        this.codeProprieteLocataire = codeProprieteLocataire;
        this.codePropriete = codePropriete;
        this.dateSignatureContrat = dateSignatureContrat;
        this.nom = nom;
        this.postNom = postNom;
        this.prenom = prenom;
    }

    @NonNull
    public String getCodeProprieteLocataire() {
        return codeProprieteLocataire;
    }

    public void setCodeProprieteLocataire(@NonNull String codeProprieteLocataire) {
        this.codeProprieteLocataire = codeProprieteLocataire;
    }

    @NonNull
    public String getCodePropriete() {
        return codePropriete;
    }

    public void setCodePropriete(@NonNull String codePropriete) {
        this.codePropriete = codePropriete;
    }

    @NonNull
    public Date getDateSignatureContrat() {
        return dateSignatureContrat;
    }

    public void setDateSignatureContrat(@NonNull Date dateSignatureContrat) {
        this.dateSignatureContrat = dateSignatureContrat;
    }

    @NonNull
    public String getNom() {
        return nom;
    }

    public void setNom(@NonNull String nom) {
        this.nom = nom;
    }

    @NonNull
    public String getPostNom() {
        return postNom;
    }

    public void setPostNom(@NonNull String postNom) {
        this.postNom = postNom;
    }

    @NonNull
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(@NonNull String prenom) {
        this.prenom = prenom;
    }
}
