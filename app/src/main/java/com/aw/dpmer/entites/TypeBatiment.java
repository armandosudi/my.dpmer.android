package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "TYPE_BATIMENT")
public class TypeBatiment {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_TYPE_BATIMENT")
    @SerializedName("CodeTypeBatiment")
    public String codeTypeBatiment;
    @NonNull
    @ColumnInfo(name = "LIBELLE")
    @SerializedName("Libelle")
    public String libelle;

    @NonNull
    public String getCodeTypeBatiment() {
        return codeTypeBatiment;
    }

    public void setCodeTypeBatiment(@NonNull String codeTypeBatiment) {
        this.codeTypeBatiment = codeTypeBatiment;
    }

    @NonNull
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(@NonNull String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return libelle;
    }
}
