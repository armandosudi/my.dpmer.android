package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.arch.persistence.room.Index;

import com.google.gson.annotations.SerializedName;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "CELLULE",foreignKeys = @ForeignKey(entity = Quartier.class,parentColumns = "CODE_QUARTIER",childColumns = "CODE_QUARTIER",onDelete = CASCADE),indices = {@Index(value = {"LIBELLE", "CODE_QUARTIER"},
        unique = true)})
public class Cellule {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_CELLULE")
    @SerializedName("CodeCellule")
    public String codeCellule;
    @NonNull
    @ColumnInfo(name = "LIBELLE")
    @SerializedName("Libelle")
    public String libelle;
    @NonNull
    @ColumnInfo(name = "CODE_QUARTIER")
    @SerializedName("CodeQuartier")
    public String codeQuartier;

    @NonNull
    public String getCodeCellule() {
        return codeCellule;
    }

    public void setCodeCellule(@NonNull String codeCellule) {
        this.codeCellule = codeCellule;
    }

    @NonNull
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(@NonNull String libelle) {
        this.libelle = libelle;
    }

    @NonNull
    public String getCodeQuartier() {
        return codeQuartier;
    }

    public void setCodeQuartier(@NonNull String codeQuartier) {
        this.codeQuartier = codeQuartier;
    }

    @Override
    public String toString() {
        return libelle;
}
}
