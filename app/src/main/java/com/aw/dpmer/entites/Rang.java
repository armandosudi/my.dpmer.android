package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "RANG")
public class Rang {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_RANG")
    public String codeRang;
    @NonNull
    public String getCodeRang() {
        return codeRang;
    }
    public void setCodeRang(@NonNull String codeRang) {
        this.codeRang = codeRang;
    }
}
