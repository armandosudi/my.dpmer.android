package com.aw.dpmer.entites;
import android.arch.persistence.room.*;
import android.support.annotation.NonNull;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "ASSUJETTI", foreignKeys = {
        @ForeignKey(entity = Cellule.class, parentColumns = "CODE_CELLULE", childColumns = "CODE_CELLULE"),
        @ForeignKey(entity = Utilisateur.class, parentColumns = "CODE_UTILISATEUR", childColumns = "CODE_UTILISATEUR")
})
public class Assujetti {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "CODE_ASSUJETTI")
    public int codeAssujetti;
    @NonNull
    @ColumnInfo(name = "NIF")
    public String nif;
    @ColumnInfo(name = "TEL1")
    public String tel1;
    @ColumnInfo(name = "TEL2")
    public String tel2;
    @ColumnInfo(name = "EMAIL")
    public String email;
    @ColumnInfo(name = "DETAILS_ADRESSE")
    public String detailsAdresse;
    @ColumnInfo(name = "CODE_CELLULE")
    public String codeCellule;
    @ColumnInfo(name = "CODE_UTILISATEUR")
    public String codeUtilisateur;

    public Assujetti(@NonNull String nif, String tel1, String tel2, String email, String detailsAdresse, String codeCellule, String codeUtilisateur) {
        this.nif = nif;
        this.tel1 = tel1;
        this.tel2 = tel2;
        this.email = email;
        this.detailsAdresse = detailsAdresse;
        this.codeCellule = codeCellule;
        this.codeUtilisateur = codeUtilisateur;
    }

    @NonNull
    public int getCodeAssujetti() {
        return codeAssujetti;
    }

    @NonNull
    public String getNif() {
        return nif;
    }

    public void setNif(@NonNull String nif) {
        this.nif = nif;
    }

    public String getTel1() {
        return tel1;
    }

    public void setTel1(String tel1) {
        this.tel1 = tel1;
    }

    public String getTel2() {
        return tel2;
    }

    public void setTel2(String tel2) {
        this.tel2 = tel2;
    }

    public String getCodeCellule() {
        return codeCellule;
    }

    public void setCodeCellule(String codeCellule) {
        this.codeCellule = codeCellule;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDetailsAdresse() {
        return detailsAdresse;
    }

    public void setDetailsAdresse(String detailsAdresse) {
        this.detailsAdresse = detailsAdresse;
    }

    public String getCodeUtilisateur() {
        return codeUtilisateur;
    }

    public void setCodeUtilisateur(String codeUtilisateur) {
        this.codeUtilisateur = codeUtilisateur;
    }
}
