package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "PERSONNE_PHYSIQUE",
        foreignKeys = @ForeignKey(entity = Profession.class,parentColumns = "CODE_PROFESSION",childColumns = "CODE_PROFESSION",onDelete = CASCADE))
public class PersonnePhysique {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_PERSONNE_PHYSIQUE")
    public String codePersonnePhysique;
    @NonNull
    @ColumnInfo(name = "NOM")
    public String nom;
    @NonNull
    @ColumnInfo(name = "POSTNOM")
    public String postnom;
    @NonNull
    @ColumnInfo(name = "PRENOM")
    public String prenom;
    @ColumnInfo(name = "DATE_NAISSANCE")
    public Date dateNaissance;
    @ColumnInfo(name = "LIEU_NAISSANCE")
    public String lieuNaissance;
    @ColumnInfo(name = "SEXE")
    public String sexe;
    @ColumnInfo(name = "PIECE_IDENTITE")
    public String pieceIdentite;
    @ColumnInfo(name = "NUMERO_PIECE_IDENTITE")
    public String numeroPieceIdentite;
    @NonNull
    @ColumnInfo(name = "NOM_PERE")
    public String nomPere;
    @ColumnInfo(name = "NOM_MERE")
    public String nomMere;
    @ColumnInfo(name = "CODE_PROFESSION")
    public String codeProfession;
    @ColumnInfo(name = "ADRESSE_POSTALE")
    public String adressePostale;
    @ColumnInfo(name = "URL_PIECE_IDENTITE")
    public String urlPieceIdentite;

    public PersonnePhysique(@NonNull String nom, @NonNull String postnom, @NonNull String prenom, Date dateNaissance, String lieuNaissance, String sexe, String pieceIdentite, String numeroPieceIdentite, @NonNull String nomPere, String nomMere, String codeProfession, String adressePostale) {
        this.nom = nom;
        this.postnom = postnom;
        this.prenom = prenom;
        this.dateNaissance = dateNaissance;
        this.lieuNaissance = lieuNaissance;
        this.sexe = sexe;
        this.pieceIdentite = pieceIdentite;
        this.numeroPieceIdentite = numeroPieceIdentite;
        this.nomPere = nomPere;
        this.nomMere = nomMere;
        this.codeProfession = codeProfession;
        this.adressePostale = adressePostale;
    }

    @NonNull
    public String getCodePersonnePhysique() {
        return codePersonnePhysique;
    }

    public void setCodePersonnePhysique(@NonNull String codePersonnePhysique) {
        this.codePersonnePhysique = codePersonnePhysique;
    }


    public String getCodeProfession() {
        return codeProfession;
    }

    public void setCodeProfession(String codeProfession) {
        this.codeProfession = codeProfession;
    }

    public String getAdressePostale() {
        return adressePostale;
    }

    public void setAdressePostale(String adressePostale) {
        this.adressePostale = adressePostale;
    }

    public String getUrlPieceIdentite() {
        return urlPieceIdentite;
    }

    public void setUrlPieceIdentite(String urlPieceIdentite) {
        this.urlPieceIdentite = urlPieceIdentite;
    }

    @NonNull
    public String getNom() {
        return nom;
    }

    public void setNom(@NonNull String nom) {
        this.nom = nom;
    }

    @NonNull
    public String getPostnom() {
        return postnom;
    }

    public void setPostnom(@NonNull String postnom) {
        this.postnom = postnom;
    }

    @NonNull
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(@NonNull String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getPieceIdentite() {
        return pieceIdentite;
    }

    public void setPieceIdentite(String pieceIdentite) {
        this.pieceIdentite = pieceIdentite;
    }

    public String getNumeroPieceIdentite() {
        return numeroPieceIdentite;
    }

    public void setNumeroPieceIdentite(String numeroPieceIdentite) {
        this.numeroPieceIdentite = numeroPieceIdentite;
    }

    @NonNull
    public String getNomPere() {
        return nomPere;
    }

    public void setNomPere(@NonNull String nomPere) {
        this.nomPere = nomPere;
    }

    public String getNomMere() {
        return nomMere;
    }

    public void setNomMere(String nomMere) {
        this.nomMere = nomMere;
    }

    @Override
    public String toString() {
        return nom ;
    }
}
