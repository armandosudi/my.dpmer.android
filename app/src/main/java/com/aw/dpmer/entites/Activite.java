package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by Sugar
 */
@Entity(tableName = "ACTIVITE", foreignKeys = {
        @ForeignKey(entity = Assujetti.class, parentColumns = "CODE_ASSUJETTI", childColumns = "CODE_ASSUJETTI"),
        @ForeignKey(entity = NatureActivite.class, parentColumns = "CODE_NATURE_ACTIVITE", childColumns = "CODE_NATURE_ACTIVITE"),
        @ForeignKey(entity = Cellule.class, parentColumns = "CODE_CELLULE", childColumns = "CODE_CELLULE"),
        @ForeignKey(entity = Utilisateur.class, parentColumns = "CODE_UTILISATEUR", childColumns = "CODE_UTILISATEUR")},
        indices = {@Index(value = {"CODE_ASSUJETTI"}, unique = true)
})
public class Activite  {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "CODE_ACTIVITE")
    public int codeActivite;
    @NonNull
    @ColumnInfo(name = "CODE_ASSUJETTI")
    public int codeAssujetti;
    @NonNull
    @ColumnInfo(name = "CODE_NATURE_ACTIVITE")
    public String codeNatureActivite;
    @NonNull
    @ColumnInfo(name = "SIGLE")
    public String sigle;
    @NonNull
    @ColumnInfo(name = "DETAILS_ADRESSE")
    public String detailsAdresse;
    @NonNull
    @ColumnInfo(name = "CODE_CELLULE")
    public String codeCellule;
    @NonNull
    @ColumnInfo(name = "ADRESSE_POSTALE")
    public String adressePostale;
    @NonNull
    @ColumnInfo(name = "IDNAT")
    public String idnat;
    @NonNull
    @ColumnInfo(name = "RCCM")
    public String rccm;
    @NonNull
    @ColumnInfo(name = "DATE_INSCRIPTION_RCCM")
    public Date dateInscripitonRccm;
    @NonNull
    @ColumnInfo(name = "DATE_DEBUT_ACTIVITE_COMMERCIALE")
    public Date dateDebutActiviteCommerciale;
    @NonNull
    @ColumnInfo(name = "MOTOS")
    public int motos;
    @NonNull
    @ColumnInfo(name = "VEHICULES_UTILITAIRES")
    public int vehiculesUtilitaires;
    @NonNull
    @ColumnInfo(name = "VEHICULES_TOURISME")
    public int vehiculesTourisme;
    @NonNull
    @ColumnInfo(name = "TRACTEURS")
    public int tracteurs;
    @NonNull
    @ColumnInfo(name = "BATEAUX")
    public int bateaux;
    @NonNull
    @ColumnInfo(name = "DROIT_IMMOBILIER")
    public String droitImmobilier;
    @NonNull
    @ColumnInfo(name = "NBR_SALARIES_NATIONAUX")
    public int salariesNationaux;
    @NonNull
    @ColumnInfo(name = "NBR_SALARIES_EXPATRIES")
    public int salariesExpatries;
    @NonNull
    @ColumnInfo(name = "URL_RCCM")
    public String urlRccm;
    @NonNull
    @ColumnInfo(name = "URL_IDNAT")
    public String urlIdnat;
    @NonNull
    @ColumnInfo(name = "DATE_ENREGISTREMENT")
    public Date dateEnregistrement;
    @NonNull
    @ColumnInfo(name = "CODE_UTILISATEUR")
    public String codeUtilisateur;
    @NonNull
    @ColumnInfo(name = "ACTIVITE_PRINCIPALE")
    public String activitePrincipale;
    @NonNull
    @ColumnInfo(name = "ACTIVITE_SECONDAIRE")
    public String activiteSecondaire;

    public Activite(@NonNull int codeAssujetti, @NonNull String codeNatureActivite, @NonNull String sigle, @NonNull String detailsAdresse, @NonNull String codeCellule, @NonNull String adressePostale, @NonNull String idnat, @NonNull String rccm, @NonNull Date dateInscripitonRccm, @NonNull Date dateDebutActiviteCommerciale, @NonNull int motos, @NonNull int vehiculesUtilitaires, @NonNull int vehiculesTourisme, @NonNull int tracteurs, @NonNull int bateaux, @NonNull String droitImmobilier, @NonNull int salariesNationaux, @NonNull int salariesExpatries, @NonNull String urlRccm, @NonNull String urlIdnat, @NonNull Date dateEnregistrement, @NonNull String codeUtilisateur, @NonNull String activitePrincipale, @NonNull String activiteSecondaire) {
        this.codeAssujetti = codeAssujetti;
        this.codeNatureActivite = codeNatureActivite;
        this.sigle = sigle;
        this.detailsAdresse = detailsAdresse;
        this.codeCellule = codeCellule;
        this.adressePostale = adressePostale;
        this.idnat = idnat;
        this.rccm = rccm;
        this.dateInscripitonRccm = dateInscripitonRccm;
        this.dateDebutActiviteCommerciale = dateDebutActiviteCommerciale;
        this.motos = motos;
        this.vehiculesUtilitaires = vehiculesUtilitaires;
        this.vehiculesTourisme = vehiculesTourisme;
        this.tracteurs = tracteurs;
        this.bateaux = bateaux;
        this.droitImmobilier = droitImmobilier;
        this.salariesNationaux = salariesNationaux;
        this.salariesExpatries = salariesExpatries;
        this.urlRccm = urlRccm;
        this.urlIdnat = urlIdnat;
        this.dateEnregistrement = dateEnregistrement;
        this.codeUtilisateur = codeUtilisateur;
        this.activitePrincipale = activitePrincipale;
        this.activiteSecondaire = activiteSecondaire;
    }

    @NonNull
    public int getCodeActivite() {
        return codeActivite;
    }

    public void setCodeActivite(@NonNull int codeActivite) {
        this.codeActivite = codeActivite;
    }

    @NonNull
    public int getCodeAssujetti() {
        return codeAssujetti;
    }

    public void setCodeAssujetti(@NonNull int codeAssujetti) {
        this.codeAssujetti = codeAssujetti;
    }

    @NonNull
    public String getCodeNatureActivite() {
        return codeNatureActivite;
    }

    public void setCodeNatureActivite(@NonNull String codeNatureActivite) {
        this.codeNatureActivite = codeNatureActivite;
    }

    @NonNull
    public String getSigle() {
        return sigle;
    }

    public void setSigle(@NonNull String sigle) {
        this.sigle = sigle;
    }

    @NonNull
    public String getDetailsAdresse() {
        return detailsAdresse;
    }

    public void setDetailsAdresse(@NonNull String detailsAdresse) {
        this.detailsAdresse = detailsAdresse;
    }

    @NonNull
    public String getCodeCellule() {
        return codeCellule;
    }

    public void setCodeCellule(@NonNull String codeCellule) {
        this.codeCellule = codeCellule;
    }

    @NonNull
    public String getAdressePostale() {
        return adressePostale;
    }

    public void setAdressePostale(@NonNull String adressePostale) {
        this.adressePostale = adressePostale;
    }

    @NonNull
    public String getIdnat() {
        return idnat;
    }

    public void setIdnat(@NonNull String idnat) {
        this.idnat = idnat;
    }

    @NonNull
    public String getRccm() {
        return rccm;
    }

    public void setRccm(@NonNull String rccm) {
        this.rccm = rccm;
    }

    @NonNull
    public Date getDateInscripitonRccm() {
        return dateInscripitonRccm;
    }

    public void setDateInscripitonRccm(@NonNull Date dateInscripitonRccm) {
        this.dateInscripitonRccm = dateInscripitonRccm;
    }

    @NonNull
    public Date getDateDebutActiviteCommerciale() {
        return dateDebutActiviteCommerciale;
    }

    public void setDateDebutActiviteCommerciale(@NonNull Date dateDebutActiviteCommerciale) {
        this.dateDebutActiviteCommerciale = dateDebutActiviteCommerciale;
    }

    @NonNull
    public int getMotos() {
        return motos;
    }

    public void setMotos(@NonNull int motos) {
        this.motos = motos;
    }

    @NonNull
    public int getVehiculesUtilitaires() {
        return vehiculesUtilitaires;
    }

    public void setVehiculesUtilitaires(@NonNull int vehiculesUtilitaires) {
        this.vehiculesUtilitaires = vehiculesUtilitaires;
    }

    @NonNull
    public int getVehiculesTourisme() {
        return vehiculesTourisme;
    }

    public void setVehiculesTourisme(@NonNull int vehiculesTourisme) {
        this.vehiculesTourisme = vehiculesTourisme;
    }

    @NonNull
    public int getTracteurs() {
        return tracteurs;
    }

    public void setTracteurs(@NonNull int tracteurs) {
        this.tracteurs = tracteurs;
    }

    @NonNull
    public int getBateaux() {
        return bateaux;
    }

    public void setBateaux(@NonNull int bateaux) {
        this.bateaux = bateaux;
    }

    @NonNull
    public String getDroitImmobilier() {
        return droitImmobilier;
    }

    public void setDroitImmobilier(@NonNull String droitImmobilier) {
        this.droitImmobilier = droitImmobilier;
    }

    @NonNull
    public int getSalariesNationaux() {
        return salariesNationaux;
    }

    public void setSalariesNationaux(@NonNull int salariesNationaux) {
        this.salariesNationaux = salariesNationaux;
    }

    @NonNull
    public int getSalariesExpatries() {
        return salariesExpatries;
    }

    public void setSalariesExpatries(@NonNull int salariesExpatries) {
        this.salariesExpatries = salariesExpatries;
    }

    @NonNull
    public String getUrlRccm() {
        return urlRccm;
    }

    public void setUrlRccm(@NonNull String urlRccm) {
        this.urlRccm = urlRccm;
    }

    @NonNull
    public String getUrlIdnat() {
        return urlIdnat;
    }

    public void setUrlIdnat(@NonNull String urlIdnat) {
        this.urlIdnat = urlIdnat;
    }

    @NonNull
    public Date getDateEnregistrement() {
        return dateEnregistrement;
    }

    public void setDateEnregistrement(@NonNull Date dateEnregistrement) {
        this.dateEnregistrement = dateEnregistrement;
    }

    @NonNull
    public String getCodeUtilisateur() {
        return codeUtilisateur;
    }

    public void setCodeUtilisateur(@NonNull String codeUtilisateur) {
        this.codeUtilisateur = codeUtilisateur;
    }

    @NonNull
    public String getActivitePrincipale() {
        return activitePrincipale;
    }

    public void setActivitePrincipale(@NonNull String activitePrincipale) {
        this.activitePrincipale = activitePrincipale;
    }

    @NonNull
    public String getActiviteSecondaire() {
        return activiteSecondaire;
    }

    public void setActiviteSecondaire(@NonNull String activiteSecondaire) {
        this.activiteSecondaire = activiteSecondaire;
    }
}
