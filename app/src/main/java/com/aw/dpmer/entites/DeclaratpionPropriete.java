package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Sugar
 */

@Entity(tableName = "DECLARATION_PROPRIETE", foreignKeys = {
        @ForeignKey(entity = Declaration.class, parentColumns = "CODE_DECLARATION", childColumns = "CODE_DECLARATION"),
        @ForeignKey(entity = Propriete.class, parentColumns = "CODE_PROPRIETE", childColumns = "CODE_PROPRIETE")})
public class DeclaratpionPropriete {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_DECLARATION_PROPRIETE")
    public String codeDeclarationPropriete;
    @NonNull
    @ColumnInfo(name = "CODE_DECLARATION")
    public String codeDeclaration;
    @NonNull
    @ColumnInfo(name = "CODE_PROPRIETE")
    public String codePropriete;

    public DeclaratpionPropriete(@NonNull String codeDeclarationPropriete, @NonNull String codeDeclaration, @NonNull String codePropriete) {
        this.codeDeclarationPropriete = codeDeclarationPropriete;
        this.codeDeclaration = codeDeclaration;
        this.codePropriete = codePropriete;
    }

    @NonNull
    public String getCodeDeclarationPropriete() {
        return codeDeclarationPropriete;
    }

    public void setCodeDeclarationPropriete(@NonNull String codeDeclarationPropriete) {
        this.codeDeclarationPropriete = codeDeclarationPropriete;
    }

    @NonNull
    public String getCodeDeclaration() {
        return codeDeclaration;
    }

    public void setCodeDeclaration(@NonNull String codeDeclaration) {
        this.codeDeclaration = codeDeclaration;
    }

    @NonNull
    public String getCodePropriete() {
        return codePropriete;
    }

    public void setCodePropriete(@NonNull String codePropriete) {
        this.codePropriete = codePropriete;
    }
}
