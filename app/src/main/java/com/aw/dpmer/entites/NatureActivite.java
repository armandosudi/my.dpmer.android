package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "NATURE_ACTIVITE")
public class NatureActivite {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_NATURE_ACTIVITE")
    @SerializedName("CodeNatureActivite")
    public String codeNatureActivite;
    @NonNull
    @ColumnInfo(name = "LIBELLE")
    @SerializedName("Libelle")
    public String libelle;

    @NonNull
    public String getCodeNatureActivite() {
        return codeNatureActivite;
    }

    public void setCodeNatureActivite(@NonNull String codeNatureActivite) {
        this.codeNatureActivite = codeNatureActivite;
    }

    @NonNull
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(@NonNull String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return libelle;
    }
}
