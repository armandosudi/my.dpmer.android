package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "COMMUNE",indices = {@Index(value = {"LIBELLE"},
        unique = true)})
public class Commune {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_COMMUNE")
    @SerializedName("CodeCommune")
    public String codeCommune;
    @ColumnInfo(name = "LIBELLE")
    @SerializedName("Libelle")
    public String libelle;
    @ColumnInfo(name = "CODE_VILLE")
    @SerializedName("CodeVille")
    public String codeVille;

    @NonNull
    public String getCodeCommune() {
        return codeCommune;
    }

    public void setCodeCommune(@NonNull String codeCommune) {
        this.codeCommune = codeCommune;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCodeVille() {
        return codeVille;
    }

    public void setCodeVille(String codeVille) {
        this.codeVille = codeVille;
    }

    @Override
    public String toString() {
        return libelle;
    }
}
