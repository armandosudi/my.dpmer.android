package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "USAGE")
public class Usage {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_USAGE")
    @SerializedName("CodeUsage")
    public String codeUsage;
    @NonNull
    @ColumnInfo(name = "LIBELLE")
    @SerializedName("Libelle")
    public String libelle;

    @NonNull
    public String getCodeUsage() {
        return codeUsage;
    }

    public void setCodeUsage(@NonNull String codeUsage) {
        this.codeUsage = codeUsage;
    }

    @NonNull
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(@NonNull String libelle) {
        this.libelle = libelle;
    }

    @Override
    public String toString() {
        return libelle;
    }
}
