package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "FORME_JURIDIQUE")
public class FormeJuridique {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_FORME_JURIDIQUE")
    @SerializedName("CodeFormeJuridique")
    public String codeFormeJuridique;
    @NonNull
    @ColumnInfo(name = "LIBELLE")
    @SerializedName("Libelle")
    public String libelle;
    @ColumnInfo(name = "SIGLE")
    @SerializedName("Sigle")
    public String sigle;

    @NonNull
    public String getCodeFormeJuridique() {
        return codeFormeJuridique;
    }

    public void setCodeFormeJuridique(@NonNull String codeFormeJuridique) {
        this.codeFormeJuridique = codeFormeJuridique;
    }

    @NonNull
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(@NonNull String libelle) {
        this.libelle = libelle;
    }

    @NonNull
    public String getSigle() {
        return sigle;
    }

    public void setSigle(@NonNull String sigle) {
        this.sigle = sigle;
    }

    @Override
    public String toString() {
        return libelle;
    }

    public FormeJuridique(@NonNull String codeFormeJuridique, @NonNull String libelle, @NonNull String sigle) {
        this.codeFormeJuridique = codeFormeJuridique;
        this.libelle = libelle;
        this.sigle = sigle;
    }
}
