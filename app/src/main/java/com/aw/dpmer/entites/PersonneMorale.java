package com.aw.dpmer.entites;

import android.arch.persistence.room.*;
import android.support.annotation.NonNull;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "PERSONNE_MORALE",foreignKeys =
        {@ForeignKey(entity = Assujetti.class,parentColumns = "CODE_ASSUJETTI",childColumns = "CODE_PERSONNE_MORALE",onDelete = CASCADE),
        @ForeignKey(entity = FormeJuridique.class,parentColumns = "CODE_FORME_JURIDIQUE",childColumns = "CODE_FORME_JURIDIQUE")},indices = {@Index(value = {"CODE_PERSONNE_MORALE"},
        unique = true)})
public class PersonneMorale {
        @PrimaryKey
        @NonNull
        @ColumnInfo(name = "CODE_PERSONNE_MORALE")
        public String codePersonneMorale;
        @NonNull
        @ColumnInfo(name = "RAISON_SOCIALE")
        public String raisonSociale;
        @NonNull
        @ColumnInfo(name = "CODE_FORME_JURIDIQUE")
        public String codeFormeJuridique;
        @NonNull
        @ColumnInfo(name = "ENSEIGNE")
        public String enseigne;
        @NonNull
        @ColumnInfo(name = "SIGLE")
        public String sigle;

    public PersonneMorale( @NonNull String raisonSociale, @NonNull String codeFormeJuridique, @NonNull String enseigne, @NonNull String sigle) {
        this.raisonSociale = raisonSociale;
        this.codeFormeJuridique = codeFormeJuridique;
        this.enseigne = enseigne;
        this.sigle = sigle;
    }

    @NonNull
    public String getSigle() {
        return sigle;
    }

    public void setSigle(@NonNull String sigle) {
        this.sigle = sigle;
    }

    @NonNull
    public String getCodePersonneMorale() {
        return codePersonneMorale;
    }

    public void setCodePersonneMorale(@NonNull String codePersonneMorale) {
        this.codePersonneMorale = codePersonneMorale;
    }

    @NonNull
    public String getRaisonSociale() {
        return raisonSociale;
    }

    public void setRaisonSociale(@NonNull String raisonSociale) {
        this.raisonSociale = raisonSociale;
    }


    @NonNull
    public String getCodeFormeJuridique() {
        return codeFormeJuridique;
    }

    public void setCodeFormeJuridique(@NonNull String codeFormeJuridique) {
        this.codeFormeJuridique = codeFormeJuridique;
    }

    @NonNull
    public String getEnseigne() {
        return enseigne;
    }

    public void setEnseigne(@NonNull String secteursActivite) {
        this.enseigne = enseigne;
    }

    @Override
    public String toString() {
        return "Personne Morale" + " " + codePersonneMorale;
    }
}
