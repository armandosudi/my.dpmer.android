package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by Sugar
 */

@Entity(tableName = "EXERCICE_FISCALE")
public class ExerciceFiscale {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ANNEE_FISCALE")
    public String anneeFiscale;
    @NonNull
    @ColumnInfo(name = "DATE_DEBUT")
    public Date dateDebut;
    @NonNull
    @ColumnInfo(name = "DATE_FIN")
    public Date dateFin;
    @NonNull
    @ColumnInfo(name = "EN_COURS")
    public boolean enCours;
    @NonNull
    @ColumnInfo(name = "CLOTURE")
    public boolean cloture;

    public ExerciceFiscale(@NonNull String anneeFiscale, @NonNull Date dateDebut, @NonNull Date dateFin, @NonNull boolean enCours, @NonNull boolean cloture) {
        this.anneeFiscale = anneeFiscale;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.enCours = enCours;
        this.cloture = cloture;
    }

    @NonNull
    public String getAnneeFiscale() {
        return anneeFiscale;
    }

    public void setAnneeFiscale(@NonNull String anneeFiscale) {
        this.anneeFiscale = anneeFiscale;
    }

    @NonNull
    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(@NonNull Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    @NonNull
    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(@NonNull Date dateFin) {
        this.dateFin = dateFin;
    }

    @NonNull
    public boolean isEnCours() {
        return enCours;
    }

    public void setEnCours(@NonNull boolean enCours) {
        this.enCours = enCours;
    }

    @NonNull
    public boolean isCloture() {
        return cloture;
    }

    public void setCloture(@NonNull boolean cloture) {
        this.cloture = cloture;
    }
}
