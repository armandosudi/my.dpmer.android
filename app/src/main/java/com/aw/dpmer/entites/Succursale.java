package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by Sugar
 */

@Entity(tableName = "SUCCURSALE", foreignKeys = {
        @ForeignKey(entity = Activite.class, parentColumns = "CODE_ACTIVITE", childColumns = "CODE_ACTIVITE"),
        @ForeignKey(entity = Cellule.class, parentColumns = "CODE_CELLULE", childColumns = "CODE_ACTIVITE")

})
public class Succursale {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_SUCCURSALE")
    public String codeSuccursale;
    @NonNull
    @ColumnInfo(name = "CODE_ACTIVITE")
    public String codeActivite;
    @NonNull
    @ColumnInfo(name = "CODE_CELLULE")
    public String codeCellule;
    @NonNull
    @ColumnInfo(name = "DETAILS_ADRESSE")
    public String detailsAdresse;
    @NonNull
    @ColumnInfo(name = "TYPE_SUCCURSALE")
    public String typeSuccursale;

    public Succursale(@NonNull String codeSuccursale, @NonNull String codeActivite, @NonNull String codeCellule, @NonNull String detailsAdresse, @NonNull String typeSuccursale) {
        this.codeSuccursale = codeSuccursale;
        this.codeActivite = codeActivite;
        this.codeCellule = codeCellule;
        this.detailsAdresse = detailsAdresse;
        this.typeSuccursale = typeSuccursale;
    }

    @NonNull
    public String getCodeSuccursale() {
        return codeSuccursale;
    }

    public void setCodeSuccursale(@NonNull String codeSuccursale) {
        this.codeSuccursale = codeSuccursale;
    }

    @NonNull
    public String getCodeActivite() {
        return codeActivite;
    }

    public void setCodeActivite(@NonNull String codeActivite) {
        this.codeActivite = codeActivite;
    }

    @NonNull
    public String getCodeCellule() {
        return codeCellule;
    }

    public void setCodeCellule(@NonNull String codeCellule) {
        this.codeCellule = codeCellule;
    }

    @NonNull
    public String getDetailsAdresse() {
        return detailsAdresse;
    }

    public void setDetailsAdresse(@NonNull String detailsAdresse) {
        this.detailsAdresse = detailsAdresse;
    }

    @NonNull
    public String getTypeSuccursale() {
        return typeSuccursale;
    }

    public void setTypeSuccursale(@NonNull String typeSuccursale) {
        this.typeSuccursale = typeSuccursale;
    }
}
