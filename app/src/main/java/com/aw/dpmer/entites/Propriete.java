package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by jean.olenga on 15/03/2018.
 */
@Entity(tableName  = "PROPRIETE",foreignKeys =
        {@ForeignKey(entity = Utilisateur.class,parentColumns = "CODE_UTILISATEUR",childColumns = "CODE_UTILISATEUR"),
                @ForeignKey(entity = Cellule.class,parentColumns = "CODE_CELLULE",childColumns = "CODE_CELLULE"),
                @ForeignKey(entity = TypeBatiment.class,parentColumns = "CODE_TYPE_BATIMENT",childColumns = "CODE_TYPE_BATIMENT"),
                @ForeignKey(entity = Usage.class,parentColumns = "CODE_USAGE",childColumns = "CODE_USAGE")},indices = {@Index(value = {"CODE_ASSUJETTI","CODE_CELLULE","CODE_USAGE"},
        unique = true)})
public class Propriete {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "CODE_PROPRIETE")
    public int codePropriete;
    @ColumnInfo(name = "REFERENCE")
    public String reference;
    @ColumnInfo(name = "NATURE_ESPACE")
    public String natureEspace;
    @NonNull
    @ColumnInfo(name = "SUPERFICIE")
    public float superficie;
    @NonNull
    @ColumnInfo(name = "DETAILS_ADRESSE")
    public String detailsAdresse;
    @NonNull
    @ColumnInfo(name = "CODE_CELLULE")
    public String codeCellule;
    @NonNull
    @ColumnInfo(name = "CODE_TYPE_BATIMENT")
    public String codeTypeBatiment;
    @NonNull
    @ColumnInfo(name = "CODE_ASSUJETTI")
    public int codeAssujetti;
    @NonNull
    @ColumnInfo(name = "NOMBRE_ETAGES")
    public int nombreEtages;
    @NonNull
    @ColumnInfo(name = "CODE_USAGE")
    public String codeUsage;
    @ColumnInfo(name = "NATURE_TAXES")
    public String natureTaxes;
    @ColumnInfo(name = "COMPTEUR_EAU")
    public String compteurEau;
    @ColumnInfo(name = "COMPTEUR_ELECTRICITE")
    public String compteurElectricite;
    @NonNull
    @ColumnInfo(name = "URL_PHOTO1")
    public String urlPhoto1;
    @ColumnInfo(name = "URL_PHOTO2")
    public String urlPhoto2;
    @ColumnInfo(name = "URL_PHOTO3")
    public String urlPhoto3;
    @ColumnInfo(name = "URL_PHOTO4")
    public String urlPhoto4;
    @NonNull
    @ColumnInfo(name = "LATITUDE")
    public int latitude;
    @NonNull
    @ColumnInfo(name = "LONGITUDE")
    public int longitude;
    @ColumnInfo(name = "DATE_ENREGISTREMENT")
    public String dateEnregistrement;
    @NonNull
    @ColumnInfo(name = "CODE_UTILISATEUR")
    public String codeUtilisateur;

    public Propriete(String reference, String natureEspace, @NonNull float superficie, @NonNull String detailsAdresse, @NonNull String codeCellule, @NonNull String codeTypeBatiment, @NonNull int codeAssujetti, @NonNull int nombreEtages, @NonNull String codeUsage, String natureTaxes, String compteurEau, String compteurElectricite, @NonNull String urlPhoto1, String urlPhoto2, String urlPhoto3, String urlPhoto4, @NonNull int latitude, @NonNull int longitude, String dateEnregistrement, @NonNull String codeUtilisateur) {
        this.codePropriete = codePropriete;
        this.reference = reference;
        this.natureEspace = natureEspace;
        this.superficie = superficie;
        this.detailsAdresse = detailsAdresse;
        this.codeCellule = codeCellule;
        this.codeTypeBatiment = codeTypeBatiment;
        this.codeAssujetti = codeAssujetti;
        this.nombreEtages = nombreEtages;
        this.codeUsage = codeUsage;
        this.natureTaxes = natureTaxes;
        this.compteurEau = compteurEau;
        this.compteurElectricite = compteurElectricite;
        this.urlPhoto1 = urlPhoto1;
        this.urlPhoto2 = urlPhoto2;
        this.urlPhoto3 = urlPhoto3;
        this.urlPhoto4 = urlPhoto4;
        this.latitude = latitude;
        this.longitude = longitude;
        this.dateEnregistrement = dateEnregistrement;
        this.codeUtilisateur = codeUtilisateur;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @NonNull
    public int getCodePropriete() {
        return codePropriete;
    }

    public void setCodePropriete(@NonNull int codePropriete) {
        this.codePropriete = codePropriete;
    }

    @NonNull
    public float getSuperficie() {
        return superficie;
    }

    public void setSuperficie(@NonNull float superficie) {
        this.superficie = superficie;
    }

    @NonNull
    public String getDetailsAdresse() {
        return detailsAdresse;
    }

    public void setDetailsAdresse(@NonNull String detailsAdresse) {
        this.detailsAdresse = detailsAdresse;
    }

    @NonNull
    public String getCodeCellule() {
        return codeCellule;
    }

    public void setCodeCellule(@NonNull String codeCellule) {
        this.codeCellule = codeCellule;
    }

    @NonNull
    public int getCodeAssujetti() {
        return codeAssujetti;
    }

    public void setCodeAssujetti(@NonNull int codeAssujetti) {
        this.codeAssujetti = codeAssujetti;
    }

    @NonNull
    public int getNombreEtages() {
        return nombreEtages;
    }

    public void setNombreEtages(@NonNull int nombreEtages) {
        this.nombreEtages = nombreEtages;
    }

    @NonNull
    public String getCodeUsage() {
        return codeUsage;
    }

    public void setCodeUsage(@NonNull String codeUsage) {
        this.codeUsage = codeUsage;
    }

    @NonNull
    public int getLongitude() {
        return longitude;
    }

    public void setLongitude(@NonNull int longitude) {
        this.longitude = longitude;
    }

    @NonNull
    public int getLatitude() {
        return latitude;
    }

    public void setLatitude(@NonNull int latitude) {
        this.latitude = latitude;
    }

}
