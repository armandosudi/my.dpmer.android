package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by Sugar
 */
@Entity(tableName = "DECLARATION", foreignKeys = {
        @ForeignKey(entity = Assujetti.class, parentColumns = "CODE_ASSUJETTI", childColumns = "CODE_ASSUJETTI"),
        @ForeignKey(entity = ExerciceFiscale.class, parentColumns = "ANNEE_FISCALE", childColumns = "ANNEE_FISCALE")}
)
public class Declaration {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_DECLARATION")
    public String codeDeclaration;
    @NonNull
    @ColumnInfo(name = "DATE_ENREGISTREMENT")
    public Date dateEnregistrement;

    @NonNull
    @ColumnInfo(name = "ANNEE_FISCALE")
    public String anneeFiscale;
    @NonNull
    @ColumnInfo(name = "MONTANT")
    public float montant;
    @NonNull
    @ColumnInfo(name = "MODE_PAIEMENT")
    public String modePaiement;
    @NonNull
    @ColumnInfo(name = "CODE_ASSUJETTI")
    public String codeAssujetti;
    @NonNull
    @ColumnInfo(name = "REFERENCE_DECLARATION")
    public String referenceDeclaration;
    @NonNull
    @ColumnInfo(name = "URL_DECLARATION")
    public String urlDeclaration;

    public Declaration(@NonNull String codeDeclaration, @NonNull Date dateEnregistrement, @NonNull String anneeFiscale, @NonNull float montant, @NonNull String modePaiement, @NonNull String codeAssujetti, @NonNull String referenceDeclaration, @NonNull String urlDeclaration) {
        this.codeDeclaration = codeDeclaration;
        this.dateEnregistrement = dateEnregistrement;
        this.anneeFiscale = anneeFiscale;
        this.montant = montant;
        this.modePaiement = modePaiement;
        this.codeAssujetti = codeAssujetti;
        this.referenceDeclaration = referenceDeclaration;
        this.urlDeclaration = urlDeclaration;
    }

    @NonNull
    public String getCodeDeclaration() {
        return codeDeclaration;
    }

    public void setCodeDeclaration(@NonNull String codeDeclaration) {
        this.codeDeclaration = codeDeclaration;
    }

    @NonNull
    public Date getDateEnregistrement() {
        return dateEnregistrement;
    }

    public void setDateEnregistrement(@NonNull Date dateEnregistrement) {
        this.dateEnregistrement = dateEnregistrement;
    }

    @NonNull
    public String getAnneeFiscale() {
        return anneeFiscale;
    }

    public void setAnneeFiscale(@NonNull String anneeFiscale) {
        this.anneeFiscale = anneeFiscale;
    }

    @NonNull
    public float getMontant() {
        return montant;
    }

    public void setMontant(@NonNull float montant) {
        this.montant = montant;
    }

    @NonNull
    public String getModePaiement() {
        return modePaiement;
    }

    public void setModePaiement(@NonNull String modePaiement) {
        this.modePaiement = modePaiement;
    }

    @NonNull
    public String getCodeAssujetti() {
        return codeAssujetti;
    }

    public void setCodeAssujetti(@NonNull String codeAssujetti) {
        this.codeAssujetti = codeAssujetti;
    }

    @NonNull
    public String getReferenceDeclaration() {
        return referenceDeclaration;
    }

    public void setReferenceDeclaration(@NonNull String referenceDeclaration) {
        this.referenceDeclaration = referenceDeclaration;
    }

    @NonNull
    public String getUrlDeclaration() {
        return urlDeclaration;
    }

    public void setUrlDeclaration(@NonNull String urlDeclaration) {
        this.urlDeclaration = urlDeclaration;
    }
}
