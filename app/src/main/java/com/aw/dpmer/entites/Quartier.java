package com.aw.dpmer.entites;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static android.arch.persistence.room.ForeignKey.CASCADE;

/**
 * Created by jean.olenga on 16/03/2018.
 */
@Entity(tableName  = "QUARTIER",foreignKeys =
        {@ForeignKey(entity = Commune.class,parentColumns = "CODE_COMMUNE",childColumns = "CODE_COMMUNE",onDelete = CASCADE),
                @ForeignKey(entity = Rang.class,parentColumns = "CODE_RANG",childColumns = "CODE_RANG")},indices = {@Index(value = {"CODE_COMMUNE", "CODE_RANG","CODE_QUARTIER"},
        unique = true)})
public class Quartier {
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "CODE_QUARTIER")
    @SerializedName("CodeQuartier")
    public String codeQuartier;
    @ColumnInfo(name = "LIBELLE")
    @SerializedName("Libelle")
    public String libelle;
    @ColumnInfo(name = "CODE_COMMUNE")
    @SerializedName("CodeCommune")
    public String codeCommune;
    @ColumnInfo(name = "CODE_RANG")
    @SerializedName("CodeRang")
    public String codeRang;

    @NonNull
    public String getCodeQuartier() {
        return codeQuartier;
    }

    public void setCodeQuartier(@NonNull String codeQuartier) {
        this.codeQuartier = codeQuartier;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCodeCommune() {
        return codeCommune;
    }

    public void setCodeCommune(String codeCommune) {
        this.codeCommune = codeCommune;
    }

    public String getCodeRang() {
        return codeRang;
    }

    public void setCodeRang(String codeRang) {
        this.codeRang = codeRang;
    }

    @Override
    public String toString() {
        return libelle;
    }
}
