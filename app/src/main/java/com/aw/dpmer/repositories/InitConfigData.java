package com.aw.dpmer.repositories;

import android.app.Activity;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.aw.dpmer.database.DpmerDatabase;
import com.aw.dpmer.entites.Cellule;
import com.aw.dpmer.entites.Commune;
import com.aw.dpmer.entites.FormeJuridique;
import com.aw.dpmer.entites.NatureActivite;
import com.aw.dpmer.entites.Profession;
import com.aw.dpmer.entites.Profil;
import com.aw.dpmer.entites.Quartier;
import com.aw.dpmer.entites.TypeBatiment;
import com.aw.dpmer.entites.Usage;
import com.aw.dpmer.entites.Utilisateur;
import com.aw.dpmer.service.MobileApi;
import com.aw.dpmer.service.MobileApiInterface;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

public class InitConfigData extends AsyncTask<String, Integer, String> {

    private android.content.Context context;

    public InitConfigData(android.content.Context context) {
        this.context = context;
    }

    private ProgressBar progressBar;

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
        this.progressBar.setVisibility(View.VISIBLE);
    }

    private static final String TAG = "InitConfigData";

    private List<Cellule> mCelluleList = null;
    private List<Commune> mCommuneList = null;
    private List<FormeJuridique> mFormeJuridiqueList = null;
    private List<NatureActivite> mNatureActiviteList = null;
    private List<Profession> mProfessionList = null;
    private List<Profil> mProfilList = null;
    private List<Quartier> mQuartierList = null;
    private List<TypeBatiment> mTypeBatimentList = null;
    private List<Usage> mUsageList = null;
    private Utilisateur mUtilisateur = null;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String message) {
        super.onPostExecute(message);
        progressBar.setVisibility(View.INVISIBLE);

        Activity activity = (Activity) context;
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        if (message != null) {
            new AlertDialog.Builder(this.context).setTitle("")
                    .setMessage(message)
                    .setCancelable(false).setPositiveButton("Ok", null)
                    .show();
            return;
        }

    }

    @Override
    protected void onProgressUpdate(Integer[] values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected String doInBackground(String... values) {

        final MobileApiInterface mobileApiInterface = MobileApi.getService();

        String macAddress = "";
        try {
            WifiManager wifimanager = (WifiManager) context.getSystemService(
                    android.content.Context.WIFI_SERVICE);

            macAddress = wifimanager.getConnectionInfo().getMacAddress();
            if (macAddress == null) {
                return "L'APPAREIL N'A PAS D'ADRESSE MAX OU LE WI-FI N'EST PAS ACTIVE";
            }
        } catch (Exception ex) {
            return "ERROR\n" + ex.getMessage();
        }

        String msg = "MAC ADDRESS :\n" + macAddress;

        try {
            Utilisateur utilisateur = new Utilisateur(null, null,
                    null, null, null, 0, macAddress);

            Response<Utilisateur> rutilisateur = mobileApiInterface.getUtilisateur(utilisateur).execute();

            if (rutilisateur != null && rutilisateur.isSuccessful()) {

                Response<List<Profil>> rprofils = mobileApiInterface.getProfilList().execute();

                if (rprofils != null && rprofils.isSuccessful()) {
                    mProfilList = rprofils.body();
                    for (int i = 0; i < mProfilList.size(); i++) {
                        try {
                            DpmerDatabase.getInstance().getIProfilDao().insert(mProfilList.get(i));
                        } catch (Exception ex) {
                        }
                    }
                }

                try {
                    mUtilisateur = rutilisateur.body();
                    DpmerDatabase.getInstance().getIUtilisateuDao().insert(mUtilisateur);
                } catch (Exception ex) {
                }

                Response<List<Commune>> rcommunes = mobileApiInterface.getCommuneList().execute();
                if (rcommunes != null && rcommunes.isSuccessful()) {
                    mCommuneList = rcommunes.body();
                    for (int i = 0; i < mCommuneList.size(); i++) {
                        try {
                            DpmerDatabase.getInstance().getICommuneDao().insert(mCommuneList.get(i));
                        } catch (Exception ex) {
                        }
                    }
                }

                Response<List<Quartier>> rquartier = mobileApiInterface.getQuartierList().execute();
                if (rquartier != null && rquartier.isSuccessful()) {
                    mQuartierList = rquartier.body();
                    for (int i = 0; i < mQuartierList.size(); i++) {
                        try {
                            DpmerDatabase.getInstance().getIQuartierDao().insert(mQuartierList.get(i));
                        } catch (Exception ex) {
                        }
                    }
                }

                Response<List<Cellule>> rcellules = mobileApiInterface.getCelluleList().execute();
                if (rcellules != null && rcellules.isSuccessful()) {
                    mCelluleList = rcellules.body();
                    for (int i = 0; i < mCelluleList.size(); i++) {
                        try {
                            DpmerDatabase.getInstance().getICelluleDao().insert(mCelluleList.get(i));
                        } catch (Exception ex) {
                        }
                    }
                }

                Response<List<FormeJuridique>> rformesjuridiques = mobileApiInterface.getFormeJuridiqueList().execute();
                if (rformesjuridiques != null && rformesjuridiques.isSuccessful()) {
                    mFormeJuridiqueList = rformesjuridiques.body();
                    for (int i = 0; i < mFormeJuridiqueList.size(); i++) {
                        try {
                            DpmerDatabase.getInstance().getIFormeJuridiqueDao().insert(mFormeJuridiqueList.get(i));
                        } catch (Exception ex) {
                        }
                    }
                }

                Response<List<NatureActivite>> rnatures = mobileApiInterface.getNatureActiviteList().execute();
                if (rnatures != null && rnatures.isSuccessful()) {
                    mNatureActiviteList = rnatures.body();
                    for (int i = 0; i < mNatureActiviteList.size(); i++) {
                        try {
                            DpmerDatabase.getInstance().getINatureActiviteDao().insert(mNatureActiviteList.get(i));
                        } catch (Exception ex) {
                        }
                    }
                }

                Response<List<Profession>> rprofessions = mobileApiInterface.getProfessionList().execute();
                if (rprofessions != null && rprofessions.isSuccessful()) {
                    mProfessionList = rprofessions.body();
                    for (int i = 0; i < mProfessionList.size(); i++) {
                        try {
                            DpmerDatabase.getInstance().getIProfessionDao().insert(mProfessionList.get(i));
                        } catch (Exception ex) {
                        }
                    }
                }

                Response<List<TypeBatiment>> rtypesbatiment = mobileApiInterface.getTypeBatimentList().execute();
                if (rtypesbatiment != null && rtypesbatiment.isSuccessful()) {
                    mTypeBatimentList = rtypesbatiment.body();
                    for (int i = 0; i < mTypeBatimentList.size(); i++) {
                        try {
                            DpmerDatabase.getInstance().getITypeBatimentDao().insert(mTypeBatimentList.get(i));
                        } catch (Exception ex) {
                        }
                    }
                }

                Response<List<Usage>> rusages = mobileApiInterface.getUsageList().execute();
                if (rusages != null && rusages.isSuccessful()) {
                    mUsageList = rusages.body();
                    for (int i = 0; i < mUsageList.size(); i++) {
                        try {
                            DpmerDatabase.getInstance().getIUsageDao().insert(mUsageList.get(i));
                        } catch (Exception ex) {
                        }
                    }
                }

                if(rutilisateur.body() != null) {
                    msg += "\n\nSUCCES DE LA CONFIGURATION\nCONNECTEZ-VOUS AVEC "
                            + rutilisateur.body().getEmail() + " AND " + rutilisateur.body().getPassword() + "";
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return msg;
    }
}
